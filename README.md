# Actually Good Recorder

AGR, de son nom complet “Actually Good Recorder” sera un outil permettant l’enregistrement de l’écran de l’utilisateur. D’autres fonctionnalités seront disponibles telles que la prise de capture d’écran et d’audio (ordinateur et/ou microphone). Le but principal étant de proposer une alternative aux logiciels payants et/ou très gourmands de mémoire, qui plaira à monsieur et madame tout le monde. Surtout à l’ère de l’enseignement à distance, ce genre d’application pourrait être très intéressant.

# Comment l'installer

Il n'y existe pas encore d'installateur pour tester directement le programme. Vous pouvez tout de même télécharger le programme en version portable dans
un fichier compressé.

Pour pouvoir tester AGR, allez dans l'onglet "Releases" sur GitLab. Vous y allez y trouver un lien pour télécharger le fichier zip.

Ensuite, il faut extraire l'archive quelque part sur votre ordinateur. Démarrez "AGR.exe".