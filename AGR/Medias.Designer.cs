﻿namespace AGR
{
    partial class Medias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Medias));
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.menuBtnGroupe = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnAjouter = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnSupprimer = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnTrier = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnTrierParPlusRecents = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnTrierParPlusVieux = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnTrierParPlusPetit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBtnTrierParPlusGros = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBtnGroupe,
            this.menuBtnTrier});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.MaximumSize = new System.Drawing.Size(0, 27);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip2.Size = new System.Drawing.Size(651, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // menuBtnGroupe
            // 
            this.menuBtnGroupe.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBtnAjouter,
            this.menuBtnSupprimer});
            this.menuBtnGroupe.Name = "menuBtnGroupe";
            this.menuBtnGroupe.Size = new System.Drawing.Size(70, 20);
            this.menuBtnGroupe.Text = "Groupes";
            this.menuBtnGroupe.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuBtnGroupe_ItemClicked);
            // 
            // menuBtnAjouter
            // 
            this.menuBtnAjouter.Name = "menuBtnAjouter";
            this.menuBtnAjouter.Size = new System.Drawing.Size(270, 22);
            this.menuBtnAjouter.Text = "Ajouter un groupe...";
            // 
            // menuBtnSupprimer
            // 
            this.menuBtnSupprimer.Name = "menuBtnSupprimer";
            this.menuBtnSupprimer.Size = new System.Drawing.Size(270, 22);
            this.menuBtnSupprimer.Text = "Supprimer le groupe sélectionné";
            // 
            // menuBtnTrier
            // 
            this.menuBtnTrier.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBtnTrierParPlusRecents,
            this.menuBtnTrierParPlusVieux,
            this.menuBtnTrierParPlusPetit,
            this.menuBtnTrierParPlusGros});
            this.menuBtnTrier.Name = "menuBtnTrier";
            this.menuBtnTrier.Size = new System.Drawing.Size(46, 20);
            this.menuBtnTrier.Text = "Trier";
            // 
            // menuBtnTrierParPlusRecents
            // 
            this.menuBtnTrierParPlusRecents.Name = "menuBtnTrierParPlusRecents";
            this.menuBtnTrierParPlusRecents.Size = new System.Drawing.Size(219, 22);
            this.menuBtnTrierParPlusRecents.Text = "Les plus récents d\'abord";
            // 
            // menuBtnTrierParPlusVieux
            // 
            this.menuBtnTrierParPlusVieux.Name = "menuBtnTrierParPlusVieux";
            this.menuBtnTrierParPlusVieux.Size = new System.Drawing.Size(219, 22);
            this.menuBtnTrierParPlusVieux.Text = "Les plus vieux d\'abord";
            // 
            // menuBtnTrierParPlusPetit
            // 
            this.menuBtnTrierParPlusPetit.Name = "menuBtnTrierParPlusPetit";
            this.menuBtnTrierParPlusPetit.Size = new System.Drawing.Size(219, 22);
            this.menuBtnTrierParPlusPetit.Text = "Les plus petits d\'abord";
            // 
            // menuBtnTrierParPlusGros
            // 
            this.menuBtnTrierParPlusGros.Name = "menuBtnTrierParPlusGros";
            this.menuBtnTrierParPlusGros.Size = new System.Drawing.Size(219, 22);
            this.menuBtnTrierParPlusGros.Text = "Les plus gros d\'abord";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 402);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(643, 370);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(643, 370);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Medias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.ClientSize = new System.Drawing.Size(651, 426);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip2);
            this.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Medias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Médias";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Medias_FormClosing);
            this.Load += new System.EventHandler(this.Medias_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem menuBtnGroupe;
        private System.Windows.Forms.ToolStripMenuItem menuBtnAjouter;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripMenuItem menuBtnSupprimer;
        private System.Windows.Forms.ToolStripMenuItem menuBtnTrier;
        private System.Windows.Forms.ToolStripMenuItem menuBtnTrierParPlusRecents;
        private System.Windows.Forms.ToolStripMenuItem menuBtnTrierParPlusVieux;
        private System.Windows.Forms.ToolStripMenuItem menuBtnTrierParPlusPetit;
        private System.Windows.Forms.ToolStripMenuItem menuBtnTrierParPlusGros;
    }
}