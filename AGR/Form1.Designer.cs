﻿namespace AGR
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnDemarrer = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statutTimer = new System.Windows.Forms.Timer(this.components);
            this.btnOptions = new System.Windows.Forms.Button();
            this.btnMedias = new System.Windows.Forms.Button();
            this.lblGroupe = new System.Windows.Forms.Label();
            this.cbGroupe = new System.Windows.Forms.ComboBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.checkDecompte = new System.Windows.Forms.CheckBox();
            this.btnCaptureEcran = new System.Windows.Forms.Button();
            this.btnAPropos = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDemarrer
            // 
            this.btnDemarrer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnDemarrer.FlatAppearance.BorderSize = 0;
            this.btnDemarrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDemarrer.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnDemarrer.Location = new System.Drawing.Point(14, 119);
            this.btnDemarrer.Name = "btnDemarrer";
            this.btnDemarrer.Size = new System.Drawing.Size(86, 30);
            this.btnDemarrer.TabIndex = 0;
            this.btnDemarrer.Text = "Démarrer";
            this.btnDemarrer.UseVisualStyleBackColor = false;
            this.btnDemarrer.Click += new System.EventHandler(this.btnDemarrer_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnPause.FlatAppearance.BorderSize = 0;
            this.btnPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPause.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnPause.Location = new System.Drawing.Point(106, 119);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(86, 30);
            this.btnPause.TabIndex = 1;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Window;
            this.statusStrip1.Font = new System.Drawing.Font("Montserrat", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 222);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(467, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(33, 17);
            this.statusLabel.Text = "Prêt";
            // 
            // statutTimer
            // 
            this.statutTimer.Interval = 1000;
            this.statutTimer.Tick += new System.EventHandler(this.statutTimer_Tick);
            // 
            // btnOptions
            // 
            this.btnOptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOptions.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnOptions.Location = new System.Drawing.Point(368, 119);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(86, 30);
            this.btnOptions.TabIndex = 3;
            this.btnOptions.Text = "Options";
            this.btnOptions.UseVisualStyleBackColor = false;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // btnMedias
            // 
            this.btnMedias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnMedias.FlatAppearance.BorderSize = 0;
            this.btnMedias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedias.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnMedias.Location = new System.Drawing.Point(199, 119);
            this.btnMedias.Name = "btnMedias";
            this.btnMedias.Size = new System.Drawing.Size(86, 30);
            this.btnMedias.TabIndex = 4;
            this.btnMedias.Text = "Médias";
            this.btnMedias.UseVisualStyleBackColor = false;
            this.btnMedias.Click += new System.EventHandler(this.btnMedias_Click);
            // 
            // lblGroupe
            // 
            this.lblGroupe.AutoSize = true;
            this.lblGroupe.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblGroupe.Location = new System.Drawing.Point(14, 190);
            this.lblGroupe.Name = "lblGroupe";
            this.lblGroupe.Size = new System.Drawing.Size(55, 16);
            this.lblGroupe.TabIndex = 5;
            this.lblGroupe.Text = "Groupe:";
            // 
            // cbGroupe
            // 
            this.cbGroupe.BackColor = System.Drawing.SystemColors.Window;
            this.cbGroupe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbGroupe.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbGroupe.FormattingEnabled = true;
            this.cbGroupe.Location = new System.Drawing.Point(77, 187);
            this.cbGroupe.Name = "cbGroupe";
            this.cbGroupe.Size = new System.Drawing.Size(377, 24);
            this.cbGroupe.TabIndex = 6;
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.InitialImage")));
            this.pbLogo.Location = new System.Drawing.Point(14, 13);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(440, 100);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLogo.TabIndex = 7;
            this.pbLogo.TabStop = false;
            // 
            // checkDecompte
            // 
            this.checkDecompte.AutoSize = true;
            this.checkDecompte.Checked = true;
            this.checkDecompte.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkDecompte.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.checkDecompte.Location = new System.Drawing.Point(14, 156);
            this.checkDecompte.Name = "checkDecompte";
            this.checkDecompte.Size = new System.Drawing.Size(91, 20);
            this.checkDecompte.TabIndex = 8;
            this.checkDecompte.Text = "Décompte";
            this.checkDecompte.UseVisualStyleBackColor = true;
            // 
            // btnCaptureEcran
            // 
            this.btnCaptureEcran.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnCaptureEcran.FlatAppearance.BorderSize = 0;
            this.btnCaptureEcran.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCaptureEcran.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCaptureEcran.Location = new System.Drawing.Point(106, 156);
            this.btnCaptureEcran.Name = "btnCaptureEcran";
            this.btnCaptureEcran.Size = new System.Drawing.Size(178, 25);
            this.btnCaptureEcran.TabIndex = 9;
            this.btnCaptureEcran.Text = "Capture d\'écran";
            this.btnCaptureEcran.UseVisualStyleBackColor = false;
            this.btnCaptureEcran.Click += new System.EventHandler(this.btnCaptureEcran_Click);
            // 
            // btnAPropos
            // 
            this.btnAPropos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnAPropos.FlatAppearance.BorderSize = 0;
            this.btnAPropos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAPropos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnAPropos.Location = new System.Drawing.Point(368, 156);
            this.btnAPropos.Name = "btnAPropos";
            this.btnAPropos.Size = new System.Drawing.Size(86, 25);
            this.btnAPropos.TabIndex = 3;
            this.btnAPropos.Text = "À propos";
            this.btnAPropos.UseVisualStyleBackColor = false;
            this.btnAPropos.Click += new System.EventHandler(this.btnAPropos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.ClientSize = new System.Drawing.Size(467, 244);
            this.Controls.Add(this.btnAPropos);
            this.Controls.Add(this.btnCaptureEcran);
            this.Controls.Add(this.checkDecompte);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.cbGroupe);
            this.Controls.Add(this.lblGroupe);
            this.Controls.Add(this.btnMedias);
            this.Controls.Add(this.btnOptions);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnDemarrer);
            this.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Actually Good Recorder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDemarrer;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.Timer statutTimer;
        private System.Windows.Forms.Button btnOptions;
        private System.Windows.Forms.Button btnMedias;
        private System.Windows.Forms.Label lblGroupe;
        private System.Windows.Forms.ComboBox cbGroupe;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.CheckBox checkDecompte;
        private System.Windows.Forms.Button btnCaptureEcran;
        private System.Windows.Forms.Button btnAPropos;
    }
}