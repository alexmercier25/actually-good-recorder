﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AGR.Classes;
using ScreenRecorderLib;

namespace AGR
{
    public partial class Form1 : Form
    {
        private Enregistreur _enregistreur;
        private RecorderOptions _options;
        private bool _enregistrementEnCours;
        private bool _enPause;
        private string _fichierOptions;
        private List<Groupe> _groupes;

        public Form1()
        {
            InitializeComponent();
            _groupes = Groupe.AvoirGroupes(null);
            foreach (var groupe in _groupes)
            {
                cbGroupe.Items.Add(groupe.Nom);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnPause.Enabled = false;
            statutTimer.Start();

            try
            {
                StreamReader reader = new StreamReader(Path.Combine(Environment.CurrentDirectory, @"Resources", "settings.json"));
                _fichierOptions = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                _fichierOptions = Resource1.defaultSettings;
            }

            _options = Options.JsonAObjetOptions(_fichierOptions);
        }

        private void btnDemarrer_Click(object sender, EventArgs e)
        {
            if (!_enregistrementEnCours)
            {
                if (checkDecompte.Checked)
                {
                    this.WindowState = FormWindowState.Minimized;
                    btnDemarrer.Enabled = false;
                    btnPause.Enabled = false;
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer(Resource1.record);
                    player.Play();
                    Thread.Sleep(3600);
                    btnDemarrer.Enabled = true;
                    btnPause.Enabled = true;
                    Enregistrer();
                }
                else
                {
                    Enregistrer();
                }
            }
            else
            {
                _enregistreur.ArreterEnregistrement();
                _enregistrementEnCours = false;
                btnDemarrer.Text = "Démarrer";
                btnPause.Enabled = false;
            }
        }

        /// <summary>
        /// Permet de débuter l'enregistrement
        /// </summary>
        public void Enregistrer()
        {
            string selectedGroupe = "";

            if (cbGroupe.SelectedItem is null)
            {
                selectedGroupe = "";
            }
            else
            {
                selectedGroupe = cbGroupe.SelectedItem.ToString();
            }

            _enregistreur = new Enregistreur(
                _options,
                $"{Environment.GetFolderPath(Environment.SpecialFolder.MyVideos)}/AGR/{selectedGroupe}",
                null
            );
            _enregistreur.RecorderOptions.IsLogEnabled = true;
            _enregistreur.RecorderOptions.LogSeverityLevel = LogLevel.Trace;
            _enregistreur.RecorderOptions.LogFilePath =
                Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "/logs.txt";
            _enregistreur.CreerEnregistrement();
            btnDemarrer.Text = "Arrêter";
            btnPause.Enabled = true;
            btnPause.Text = "Pause";
            _enregistrementEnCours = true;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            if (!_enPause)
            {
                _enPause = true;
                _enregistreur.Pause();
                btnPause.Text = "Reprendre";
            }
            else
            {
                _enPause = false;
                _enregistreur.Reprendre();
                btnPause.Text = "Pause";
            }
        }

        /// <summary>
        /// Permet de mettre à jour l'affichage du status de l'enregistreur.
        /// </summary>
        private void MettreAJourStatut()
        {
            if (_enregistreur != null)
                statusLabel.Text = _enregistreur.StatutToString();
        }

        private void statutTimer_Tick(object sender, EventArgs e)
        {
            MettreAJourStatut();
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {
            Options options = new Options();
            options.Show();
        }

        private void btnAPropos_Click(object sender, EventArgs e)
        {
            AboutBox1 aPropos = new AboutBox1();
            aPropos.Show();
        }

        private void btnMedias_Click(object sender, EventArgs e)
        {
            Medias fenetreMedias = new Medias();
            var result = fenetreMedias.ShowDialog();

            if (result == DialogResult.OK)
            {
                _groupes.Clear();
                cbGroupe.Items.Clear();
                _groupes = Groupe.AvoirGroupes(null);
                foreach (var groupe in _groupes)
                {
                    cbGroupe.Items.Add(groupe.Nom);
                }
            }
        }

        private void btnCaptureEcran_Click(object sender, EventArgs e)
        {
            CaptureEcran captureEcran = new CaptureEcran();
            captureEcran.Show();
        }
    }
}
