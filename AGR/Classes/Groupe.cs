﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xabe.FFmpeg;

namespace AGR.Classes
{
    public class Groupe
    {
        private string _nom;
        private string _description;
        private List<Fichier> _fichiers;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public List<Fichier> Fichiers
        {
            get { return _fichiers; }
            set { _fichiers = value; }
        }

        public Groupe(string nom, string description)
        {
            Nom = nom;
            Description = description;
            Fichiers = new List<Fichier>();

        }

        public void TrierParDate(bool ascendant)
        {
            if (ascendant)
            {
                Fichiers = Fichiers.OrderBy(o => o.DateCreation).ToList();
            }
            else
            {
                Fichiers = Fichiers.OrderByDescending(o => o.DateCreation).ToList();
            }
        }

        public void TrierParTaille(bool ascendant)
        {
            if (ascendant)
            {
                Fichiers = Fichiers.OrderBy(o => o.Taille).ToList();
            }
            else
            {
                Fichiers = Fichiers.OrderByDescending(o => o.Taille).ToList();
            }
        }

        public static List<Groupe> AvoirGroupes(string chemin)
        {
            string path = chemin ?? Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "/AGR";
            Directory.CreateDirectory(path);
            string[] dossiers = Directory.GetDirectories(path);
            List<Groupe> groupes = new List<Groupe>();

            foreach (string dossier in dossiers)
            {
                string nomDossier = dossier.Substring(path.Length - 4).Remove(0, 5);
                StreamReader fichierDesc = new StreamReader($"{path}/{nomDossier}/description.txt");
                String desc = fichierDesc.ReadToEnd();
                fichierDesc.Close();
                groupes.Add(new Groupe(nomDossier, desc));
            }

            return groupes;
        }

        public async Task ChargerFichiers(string chemin)
        {
            string path = chemin ?? Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + $"/AGR/{Nom}";
            List<string> fichiers = Directory.GetFiles(path).ToList();
            foreach (var f in fichiers)
            {
                if (f.Contains("description.txt"))
                {
                    //fichiers.Remove(f);
                }
                else
                {
                    FileInfo fileInfo = new FileInfo(f);
                    switch (fileInfo.Extension)
                    {
                        case ".mp4":
                            IMediaInfo mediaInfo = await FFmpeg.GetMediaInfo(fileInfo.FullName);
                            IVideoStream videoStream = mediaInfo.VideoStreams.FirstOrDefault();
                            Fichiers.Add(new Video(
                                fileInfo.Name,
                                "",
                                fileInfo.Length,
                                fileInfo.CreationTime,
                                fileInfo.Extension,
                                mediaInfo.Duration,
                                videoStream.Framerate,
                                new System.Drawing.Size(videoStream.Width, videoStream.Height),
                                fileInfo.FullName
                                ));
                            if (!File.Exists(Path.ChangeExtension(fileInfo.FullName, ".png")))
                            {
                                await Video.CreerMiniature(fileInfo, videoStream);
                            }
                            break;
                        case ".jpg":
                            var img = System.Drawing.Image.FromFile(fileInfo.FullName);
                            Fichiers.Add(new Image(
                                    fileInfo.Name,
                                    "",
                                    fileInfo.Length,
                                    fileInfo.CreationTime,
                                    fileInfo.Extension,
                                    new System.Drawing.Size(img.Width, img.Height),
                                    fileInfo.FullName
                                ));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void Creer(string chemin)
        {
            string path = chemin ?? Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "/AGR";
            Directory.CreateDirectory($"{path}/{Nom}");
            FileStream fileStream = File.Create($"{path}/{Nom}/description.txt");
            fileStream.Close();
            StreamWriter fichierDesc = new StreamWriter($"{path}/{Nom}/description.txt");
            fichierDesc.WriteLine(Description);
            fichierDesc.Dispose();
            fichierDesc.Close();
        }

        public void Supprimer(string chemin)
        {
            string path = chemin ?? Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + $"/AGR/{Nom}";
            Directory.Delete(path, true);
        }
    }
}
