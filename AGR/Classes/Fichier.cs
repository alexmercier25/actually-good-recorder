﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AGR.Classes
{
    public abstract class Fichier
    {
        private string _nom;
        private string _tag;
        private long _taille;
        private DateTime _dateCreation;
        private string _extension;
        private string _chemin;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public long Taille
        {
            get { return _taille; }
            set { _taille = value; }
        }

        public DateTime DateCreation
        {
            get { return _dateCreation; }
            set { _dateCreation = value; }
        }

        public string Extension
        {
            get { return _extension; }
            set { _extension = value; }
        }

        protected Fichier(string pNom, string pTag, long pTaille, DateTime pDateCreation, string pExtension, string pChemin)
        {
            Nom = pNom;
            Tag = pTag;
            Taille = pTaille;
            DateCreation = pDateCreation;
            Extension = pExtension;
            Chemin = pChemin;
        }

        public string Chemin
        {
            get { return _chemin; }
            set { _chemin = value; }
        }

        /// <summary>
        /// Permet de supprimer le fichier
        /// </summary>
        public void Supprimer()
        {
            File.Delete(Chemin);
        }

        /// <summary>
        /// Permet de renommer un fichier
        /// </summary>
        /// <param name="nouveauNom"></param>
        public void Renommer(string nouveauNom)
        {
            File.Move(Nom, nouveauNom, true);
        }
    }
}
