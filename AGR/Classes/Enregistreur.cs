﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScreenRecorderLib;

namespace AGR.Classes
{
    public class Enregistreur
    {
        private Recorder _recorder;
        private RecorderOptions _recorderOptions;
        private string _cheminSortie;
        private string _erreur;
        private RecorderStatus _statut;

        #region Accesseurs

        public Recorder Recorder
        {
            get { return _recorder; }
            set { _recorder = value; }
        }

        public RecorderOptions RecorderOptions
        {
            get { return _recorderOptions; }
            set { _recorderOptions = value; }
        }

        public string CheminSortie
        {
            get { return _cheminSortie; }
            set { _cheminSortie = value; }
        }

        public string Erreur
        {
            get { return _erreur; }
            set { _erreur = value; }
        }

        public RecorderStatus Statut
        {
            get { return _statut; }
            set { _statut = value; }
        }

        #endregion

        #region Constructeur

        public Enregistreur(RecorderOptions recorderOptions, string cheminSortie, string nomFichier)
        {
            RecorderOptions = recorderOptions ?? new RecorderOptions()
            {
                RecorderMode = RecorderMode.Video,
                IsThrottlingDisabled = false,
                IsHardwareEncodingEnabled = true,
                IsLowLatencyEnabled = false,
                IsMp4FastStartEnabled = false,
                AudioOptions = new AudioOptions
                {
                    Bitrate = AudioBitrate.bitrate_128kbps,
                    Channels = AudioChannels.Stereo,
                    IsAudioEnabled = true,
                    IsInputDeviceEnabled = true,
                    AudioInputDevice = ""
                },
                VideoOptions = new VideoOptions
                {
                    BitrateMode = BitrateControlMode.UnconstrainedVBR,
                    Bitrate = 8000 * 1000,
                    Framerate = 60,
                    IsFixedFramerate = true,
                    EncoderProfile = H264Profile.Main
                },
                MouseOptions = new MouseOptions
                {
                    IsMouseClicksDetected = true,
                    MouseClickDetectionColor = "#FFFF00",
                    MouseRightClickDetectionColor = "#FFFF00",
                    MouseClickDetectionRadius = 30,
                    MouseClickDetectionDuration = 100,

                    IsMousePointerEnabled = true,
                    MouseClickDetectionMode = MouseDetectionMode.Hook
                }
            };

            nomFichier ??=
                $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}-{DateTime.Now.Minute}-{DateTime.Now.Second}-{DateTime.Now.Millisecond}";
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "/AGR");
            CheminSortie = $"{cheminSortie}/{nomFichier}.mp4" ?? Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + $"/AGR/{nomFichier}.mp4";
        }

        #endregion

        #region Méthodes

        [DllImport("winmm.dll")]
        private static extern long mciSendString(string command, StringBuilder retString, int ReturnLength,
            IntPtr callback);

        /// <summary>
        /// Permet de créer l'enregistreur et de débuter l'enregistrement dans un fichier.
        /// </summary>

        public void CreerEnregistrement()
        {
            _recorder = Recorder.CreateRecorder(RecorderOptions);
            _recorder.OnRecordingComplete += EnregistrementTermine;
            _recorder.OnRecordingFailed += EnregistrementEchoue;
            _recorder.OnStatusChanged += StatutChange;
            _recorder.Record(CheminSortie);
        }

        /// <summary>
        /// Permet d'arrêter l'enregistrement.
        /// </summary>
        public void ArreterEnregistrement()
        {
            _recorder.Stop();
        }

        /// <summary>
        /// Garde le chemin de sortie dans l'attribut correspondant, quand l'enregistrement se termine.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnregistrementTermine(object sender, RecordingCompleteEventArgs e)
        {
            CheminSortie = e.FilePath;
        }

        /// <summary>
        /// Permet d'avoir une erreur si l'enregistrement échoue.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnregistrementEchoue(object sender, RecordingFailedEventArgs e)
        {
            Erreur = e.Error;
        }

        /// <summary>
        /// Quand le status change, on change l'attribut correspondant.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StatutChange(object sender, RecordingStatusEventArgs e)
        {
            Statut = e.Status;
        }

        /// <summary>
        /// Permet de mettre l'enregistrement sur pause s'il ne l'est pas déjà.
        /// </summary>
        public void Pause()
        {
            if (_recorder.Status == RecorderStatus.Recording)
                _recorder.Pause();
        }

        /// <summary>
        /// Permet de reprendre l'enregistrement s'il est en pause.
        /// </summary>
        public void Reprendre()
        {
            if (_recorder.Status == RecorderStatus.Paused)
                _recorder.Resume();
        }

        /// <summary>
        /// Permet d'avoir une version plus lisible du status, pour l'affichage.
        /// </summary>
        /// <returns></returns>
        public string StatutToString()
        {
            string str = "";

            switch (Statut)
            {
                case RecorderStatus.Recording:
                    str = "Enregistrement en cours..";
                    break;
                case RecorderStatus.Finishing:
                    str = "Finalisation de l'enregistrement...";
                    break;
                case RecorderStatus.Idle:
                    str = "Prêt";
                    break;
                case RecorderStatus.Paused:
                    str = "Enregistrement en pause";
                    break;
            }

            return str;
        }

        #endregion
    }
}
