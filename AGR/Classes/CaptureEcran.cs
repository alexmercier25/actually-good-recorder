﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace AGR.Classes
{
    public partial class CaptureEcran : Form
    {
        int selectX;
        int selectY;
        int selectWidth;
        int selectHeight;
        public Pen selectPen;
        bool start = false;
        public CaptureEcran()
        {
            InitializeComponent();
        }

        private void CaptureEcran_Load(object sender, EventArgs e)
        {
            this.Top = 0;
            this.Left = 0;
            this.WindowState = FormWindowState.Maximized;
            pictureBox.Location = new Point(0, 0);
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox.MouseMove += PictureBox_MouseMove;
            pictureBox.MouseDown += PictureBox_MouseDown;

            this.Hide();

            Screen currentScreen = Screen.FromControl(this);

            Bitmap printscreen = new Bitmap(currentScreen.Bounds.Width,
                                     currentScreen.Bounds.Height);

            Graphics graphics = Graphics.FromImage(printscreen as System.Drawing.Image);

            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);

            using (MemoryStream s = new MemoryStream())
            {
                printscreen.Save(s, ImageFormat.Bmp);
                pictureBox.Size = new System.Drawing.Size(this.Width, this.Height);
                pictureBox.Image = System.Drawing.Image.FromStream(s);
            }

            this.Show();

            Cursor = Cursors.Cross;
        }

        async private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (!start)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    selectX = e.X;
                    selectY = e.Y;
                    selectPen = new Pen(Color.Red, 1);
                    selectPen.DashStyle = DashStyle.DashDotDot;
                }

                pictureBox.Refresh();

                start = true;
            }
            else
            {

                if (pictureBox.Image == null)
                    return;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    pictureBox.Refresh();
                    selectWidth = e.X - selectX;
                    selectHeight = e.Y - selectY;
                    pictureBox.CreateGraphics().DrawRectangle(selectPen, selectX,
                             selectY, selectWidth, selectHeight);

                }
                start = false;

                PostCapture postCapture = new PostCapture();
                var result = postCapture.ShowDialog();

                if (result == DialogResult.OK)
                {
                    switch (postCapture.Action)
                    {
                        case ActionCaptureEcran.Copier:
                            SaveToClipboard();
                            this.Close();
                            break;
                        case ActionCaptureEcran.Sauvegarder:
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            saveFileDialog.Filter = "Fichiers image (*.jpg)|*.jpg";
                            saveFileDialog.FileName = "Capture";
                            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                            {
                                if (selectWidth > 0)
                                {
                                    Rectangle rect = new Rectangle(selectX, selectY, selectWidth, selectHeight);

                                    Bitmap OriginalImage = new Bitmap(pictureBox.Image, pictureBox.Width, pictureBox.Height);

                                    Bitmap _img = new Bitmap(selectWidth, selectHeight);

                                    Graphics g = Graphics.FromImage(_img);

                                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                                    g.DrawImage(OriginalImage, 0, 0, rect, GraphicsUnit.Pixel);

                                    _img.Save(saveFileDialog.FileName, ImageFormat.Jpeg);
                                    this.Close();
                                }
                            }
                            break;
                        case ActionCaptureEcran.SauvegarderGroupe:
                            Rectangle rect2 = new Rectangle(selectX, selectY, selectWidth, selectHeight);

                            Bitmap OriginalImage2 = new Bitmap(pictureBox.Image, pictureBox.Width, pictureBox.Height);

                            Bitmap _img2 = new Bitmap(selectWidth, selectHeight);
                            
                            Graphics g2 = Graphics.FromImage(_img2);

                            g2.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            g2.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                            g2.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            g2.DrawImage(OriginalImage2, 0, 0, rect2, GraphicsUnit.Pixel);

                            string nomFichier = $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}-{DateTime.Now.Minute}-{DateTime.Now.Second}-{DateTime.Now.Millisecond}";
                            _img2.Save(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + $"/AGR/{postCapture.GroupeSelectionne.Nom}/{nomFichier}.jpg", ImageFormat.Jpeg);
                            this.Close();
                            break;
                        case ActionCaptureEcran.MettreEnLigne:
                            Rectangle rect3 = new Rectangle(selectX, selectY, selectWidth, selectHeight);

                            Bitmap OriginalImage3 = new Bitmap(pictureBox.Image, pictureBox.Width, pictureBox.Height);

                            Bitmap _img3 = new Bitmap(selectWidth, selectHeight);

                            Graphics g3 = Graphics.FromImage(_img3);

                            g3.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            g3.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                            g3.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            g3.DrawImage(OriginalImage3, 0, 0, rect3, GraphicsUnit.Pixel);

                            string nomFichier2 = $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}-{DateTime.Now.Minute}-{DateTime.Now.Second}-{DateTime.Now.Millisecond}";
                            _img3.Save(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + $"/AGR/{nomFichier2}.jpg", ImageFormat.Jpeg);
                            Image image = new Image(nomFichier2, "", 1, DateTime.Now, "jpg", new Size(selectWidth, selectHeight), Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + $"/AGR/{nomFichier2}.jpg");
                            await image.MettreSurImgur();
                            this.Close();
                            break;
                        default:
                            this.Close();
                            break;
                    }
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox.Image == null)
                return;
            if (start)
            {
                pictureBox.Refresh();

                selectWidth = e.X - selectX;
                selectHeight = e.Y - selectY;

                pictureBox.CreateGraphics().DrawRectangle(selectPen,
                          selectX, selectY, selectWidth, selectHeight);
            }
        }

        /// <summary>
        /// Sauvegarde l'image dans le presse-papier
        /// </summary>
        private void SaveToClipboard()
        {
            if (selectWidth > 0)
            {
                Rectangle rect = new Rectangle(selectX, selectY, selectWidth, selectHeight);

                Bitmap OriginalImage = new Bitmap(pictureBox.Image, pictureBox.Width, pictureBox.Height);

                Bitmap _img = new Bitmap(selectWidth, selectHeight);

                Graphics g = Graphics.FromImage(_img);

                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(OriginalImage, 0, 0, rect, GraphicsUnit.Pixel);

                Clipboard.SetImage(_img);
            }
        }
    }
}
