﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Xabe.FFmpeg;

namespace AGR.Classes
{
    public class Video : Fichier
    {
        private TimeSpan _duree;
        private double _ips;
        private Size _resolution;

        public TimeSpan Duree
        {
            get { return _duree; }
            set { _duree = value; }
        }

        public double Ips
        {
            get { return _ips; }
            set { _ips = value; }
        }

        public Size Resolution
        {
            get { return _resolution; }
            set { _resolution = value; }
        }

        public Video(string pNom, string pTag, long pTaille, DateTime pDateCreation, string pExtension, TimeSpan pDuree, double pIps, Size pResolution, string pChemin)
            : base(pNom, pTag, pTaille, pDateCreation, pExtension, pChemin)
        {
            Duree = pDuree;
            Ips = pIps;
            Resolution = pResolution;
        }

        /// <summary>
        /// Permet de créer une miniature pour la vidéo
        /// </summary>
        /// <param name="fileInfo">Infos du fichier</param>
        /// <param name="videoStream">Flux vidéo</param>
        /// <returns>Une tâche pouvant être attendue</returns>
        public static async Task CreerMiniature(FileInfo fileInfo, IVideoStream videoStream)
        {
            Func<string, string> outputFileNameBuilder = (number) => { return Path.ChangeExtension(fileInfo.FullName, ".png"); };
            IConversionResult conversionResult = await FFmpeg.Conversions.New()
                .AddStream(videoStream)
                .ExtractNthFrame(1, outputFileNameBuilder)
                .Start();
        }

        /// <summary>
        /// Permet de couper une vidéo
        /// </summary>
        /// <param name="videoStream">Le flux vidéo</param>
        /// <param name="startTime">Le temps de départ</param>
        /// <param name="duree">La durée</param>
        /// <returns></returns>
        public async static Task CouperVideo(IVideoStream videoStream, TimeSpan startTime, TimeSpan duree)
        {
            IConversion conversion = await FFmpeg.Conversions.FromSnippet.Split(videoStream.Path, "C:\\Users\\ip0d3\\Desktop\\Workspace", startTime, duree);
            videoStream.Split(startTime, duree);
            IConversionResult result = await conversion.Start();
            MessageBox.Show(result.Duration.TotalSeconds.ToString());
        }

        /// <summary>
        /// Permet de lire une vidéo
        /// </summary>
        public void Lire()
        {
            Process.Start(Chemin);
        }
    }
}
