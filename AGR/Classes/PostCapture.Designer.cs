﻿namespace AGR.Classes
{
    partial class PostCapture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PostCapture));
            this.label1 = new System.Windows.Forms.Label();
            this.btnCopier = new System.Windows.Forms.Button();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.Annuler = new System.Windows.Forms.Button();
            this.btnEnregistrerGroupe = new System.Windows.Forms.Button();
            this.cbGroupes = new System.Windows.Forms.ComboBox();
            this.btnImgur = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Que voulez-vous faire avec l\'image ?";
            // 
            // btnCopier
            // 
            this.btnCopier.Location = new System.Drawing.Point(12, 54);
            this.btnCopier.Name = "btnCopier";
            this.btnCopier.Size = new System.Drawing.Size(75, 23);
            this.btnCopier.TabIndex = 1;
            this.btnCopier.Text = "Copier";
            this.btnCopier.UseVisualStyleBackColor = true;
            this.btnCopier.Click += new System.EventHandler(this.btnCopier_Click);
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(93, 54);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(75, 23);
            this.btnEnregistrer.TabIndex = 2;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // Annuler
            // 
            this.Annuler.Location = new System.Drawing.Point(174, 54);
            this.Annuler.Name = "Annuler";
            this.Annuler.Size = new System.Drawing.Size(75, 23);
            this.Annuler.TabIndex = 3;
            this.Annuler.Text = "Annuler";
            this.Annuler.UseVisualStyleBackColor = true;
            this.Annuler.Click += new System.EventHandler(this.Annuler_Click);
            // 
            // btnEnregistrerGroupe
            // 
            this.btnEnregistrerGroupe.Location = new System.Drawing.Point(12, 83);
            this.btnEnregistrerGroupe.Name = "btnEnregistrerGroupe";
            this.btnEnregistrerGroupe.Size = new System.Drawing.Size(237, 23);
            this.btnEnregistrerGroupe.TabIndex = 4;
            this.btnEnregistrerGroupe.Text = "Enregistrer dans le groupe :";
            this.btnEnregistrerGroupe.UseVisualStyleBackColor = true;
            this.btnEnregistrerGroupe.Click += new System.EventHandler(this.btnEnregistrerGroupe_Click);
            // 
            // cbGroupes
            // 
            this.cbGroupes.FormattingEnabled = true;
            this.cbGroupes.Location = new System.Drawing.Point(12, 112);
            this.cbGroupes.Name = "cbGroupes";
            this.cbGroupes.Size = new System.Drawing.Size(237, 23);
            this.cbGroupes.TabIndex = 5;
            this.cbGroupes.Text = "Sélectionnez un groupe...";
            // 
            // btnImgur
            // 
            this.btnImgur.Location = new System.Drawing.Point(12, 141);
            this.btnImgur.Name = "btnImgur";
            this.btnImgur.Size = new System.Drawing.Size(237, 23);
            this.btnImgur.TabIndex = 1;
            this.btnImgur.Text = "Mettre en ligne sur Imgur";
            this.btnImgur.UseVisualStyleBackColor = true;
            this.btnImgur.Click += new System.EventHandler(this.btnCopier_Click);
            // 
            // PostCapture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 173);
            this.Controls.Add(this.btnImgur);
            this.Controls.Add(this.cbGroupes);
            this.Controls.Add(this.btnEnregistrerGroupe);
            this.Controls.Add(this.Annuler);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.btnCopier);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PostCapture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Capture d\'écran réussie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCopier;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.Button Annuler;
        private System.Windows.Forms.Button btnEnregistrerGroupe;
        private System.Windows.Forms.ComboBox cbGroupes;
        private System.Windows.Forms.Button btnImgur;
    }
}