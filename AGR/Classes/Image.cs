﻿using Imgur.API.Authentication;
using Imgur.API.Endpoints;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AGR.Classes
{
    public class Image : Fichier
    {
        private Size _resolution;

        public Size Resolution
        {
            get { return _resolution; }
            set { _resolution = value; }
        }

        public Image(string pNom, string pTag, long pTaille, DateTime pDateCreation, string pExtension, Size resolution, string pChemin) : base(pNom, pTag, pTaille, pDateCreation, pExtension, pChemin)
        {
            Resolution = resolution;
        }

        /// <summary>
        /// Permet de mettre en ligne une image sur Imgur.com
        /// </summary>
        async public Task MettreSurImgur()
        {
            var apiClient = new ApiClient("f0cdb00ce940143");
            var httpClient = new HttpClient();

            var filePath = Chemin;
            using var fileStream = File.OpenRead(filePath);

            var imageEndpoint = new ImageEndpoint(apiClient, httpClient);
            var imageUpload = await imageEndpoint.UploadImageAsync(fileStream);
            Clipboard.SetText(imageUpload.Link);
            MessageBox.Show("Le lien de votre image a été copié dans votre presse-papier.");
        }
    }
}
