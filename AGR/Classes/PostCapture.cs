﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AGR.Classes
{
    public enum ActionCaptureEcran
    {
        Copier,
        Sauvegarder,
        SauvegarderGroupe,
        MettreEnLigne
    }

    public partial class PostCapture : Form
    {
        private ActionCaptureEcran _action;
        private List<Groupe> _groupes;
        private Groupe _groupeSelectionne;

        public ActionCaptureEcran Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
            }
        }

        public Groupe GroupeSelectionne
        {
            get
            {
                return _groupeSelectionne;
            }
            set
            {
                _groupeSelectionne = value;
            }
        }

        public PostCapture()
        {
            InitializeComponent();
            _groupes = Groupe.AvoirGroupes(null);
            foreach (var groupe in _groupes)
            {
                cbGroupes.Items.Add(groupe.Nom);
            }
            btnImgur.Click += BtnImgur_Click;
        }

        private void BtnImgur_Click(object sender, EventArgs e)
        {
            Action = ActionCaptureEcran.MettreEnLigne;
            this.DialogResult = DialogResult.OK;
        }

        private void btnCopier_Click(object sender, EventArgs e)
        {
            Action = ActionCaptureEcran.Copier;
            this.DialogResult = DialogResult.OK;
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            Action = ActionCaptureEcran.Sauvegarder;
            this.DialogResult = DialogResult.OK;
        }

        private void Annuler_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnEnregistrerGroupe_Click(object sender, EventArgs e)
        {
            if (cbGroupes.SelectedIndex != -1)
            {
                Action = ActionCaptureEcran.SauvegarderGroupe;
                GroupeSelectionne = _groupes.Where((g) => g.Nom == cbGroupes.SelectedItem.ToString()).First();
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
