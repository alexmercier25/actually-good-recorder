﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xabe.FFmpeg;

namespace AGR
{
    public partial class CouperVideo : Form
    {
        private TimeSpan _dureeTotale;
        const string FORMAT = "HH:mm:ss";
        private TimeSpan _de;
        private TimeSpan _a;

        public TimeSpan DureeTotale
        {
            get { return _dureeTotale; }
            set { _dureeTotale = value; }
        }

        public TimeSpan De
        {
            get { return _de; }
            set { _de = value; }
        }

        public TimeSpan A
        {
            get { return _a; }
            set { _a = value; }
        }

        public CouperVideo()
        {
            InitializeComponent();
        }

        private void CouperVideo_Load(object sender, EventArgs e)
        {
            txtA.Text = DureeTotale.ToString(@"hh\:mm\:ss");
            txtDe.Text = "00:00:00";
        }

        private void btnRaccourcir_Click(object sender, EventArgs e)
        {
            CultureInfo invariant = System.Globalization.CultureInfo.InvariantCulture;

            if (TimeSpan.TryParseExact(txtDe.Text, @"hh\:mm\:ss", invariant, out _de))
            {
                if (TimeSpan.TryParseExact(txtA.Text, @"hh\:mm\:ss", invariant, out _a))
                {
                    if (De < A)
                    {
                        if (A <= DureeTotale)
                        {
                            if (De <= DureeTotale)
                            {
                                this.DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                errorProvider1.SetError(txtDe, "Plus grand que la durée totale actuelle!");
                            }
                        }
                        else
                        {
                            errorProvider1.SetError(txtA, "Plus grand que la durée totale actuelle!");
                        }
                    } else
                    {
                        errorProvider1.SetError(txtDe, "Le début est après l'arrivée!");
                    }
                } else
                {
                    errorProvider1.SetError(txtDe, "Veuillez entrer du texte sous forme HH:mm:ss");
                }
            } else
            {
                errorProvider1.SetError(txtDe, "Veuillez entrer du texte sous forme HH:mm:ss");
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
