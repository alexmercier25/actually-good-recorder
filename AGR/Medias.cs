﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AGR.Classes;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Downloader;
using Image = AGR.Classes.Image;

namespace AGR
{
    public partial class Medias : Form
    {
        private List<Groupe> _groupes;
        private List<string> _groupesChargees;
        public Medias()
        {
            InitializeComponent();
            _groupesChargees = new List<string>();
            _groupes = Groupe.AvoirGroupes(null);
            menuBtnTrier.DropDownItemClicked += menuBtnTrier_ItemClicked;
        }

        private void Medias_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ChargerGroupes();
        }

        private void menuBtnTrier_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (_groupes.Count > 0)
            {
                if (e.ClickedItem == menuBtnTrierParPlusRecents)
                {
                    Groupe groupe = _groupes.First(e => e.Nom == tabControl1.SelectedTab.Text.Split(" - ")[0]);
                    groupe.TrierParDate(false);
                    tabControl1.SelectedTab.Controls.Clear();
                    ChargerContenuGroupe(groupe, true, tabControl1.SelectedTab);
                }
                else if (e.ClickedItem == menuBtnTrierParPlusVieux)
                {
                    Groupe groupe = _groupes.First(e => e.Nom == tabControl1.SelectedTab.Text.Split(" - ")[0]);
                    groupe.TrierParDate(true);
                    tabControl1.SelectedTab.Controls.Clear();
                    ChargerContenuGroupe(groupe, true, tabControl1.SelectedTab);
                }
                else if (e.ClickedItem == menuBtnTrierParPlusPetit)
                {
                    Groupe groupe = _groupes.First(e => e.Nom == tabControl1.SelectedTab.Text.Split(" - ")[0]);
                    groupe.TrierParTaille(true);
                    tabControl1.SelectedTab.Controls.Clear();
                    ChargerContenuGroupe(groupe, true, tabControl1.SelectedTab);
                }
                else if (e.ClickedItem == menuBtnTrierParPlusGros)
                {
                    Groupe groupe = _groupes.First(e => e.Nom == tabControl1.SelectedTab.Text.Split(" - ")[0]);
                    groupe.TrierParTaille(false);
                    tabControl1.SelectedTab.Controls.Clear();
                    ChargerContenuGroupe(groupe, true, tabControl1.SelectedTab);
                }
            }
        }

        private void menuBtnGroupe_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == menuBtnAjouter)
            {
                AjouterGroupe fenetreAjouterGroupe = new AjouterGroupe();
                DialogResult result = fenetreAjouterGroupe.ShowDialog();

                if (result == DialogResult.OK)
                {
                    Groupe nouveauGroupe = fenetreAjouterGroupe.Groupe;
                    nouveauGroupe.Creer(null);
                    _groupes.Add(nouveauGroupe);
                    TabPage tabPage = new TabPage();
                    tabPage.Text = nouveauGroupe.Nom;
                    tabControl1.Controls.Add(tabPage);
                    _groupesChargees.Add(nouveauGroupe.Nom);
                    //ChargerGroupes(true);
                }
            }
            else if (e.ClickedItem == menuBtnSupprimer)
            {
                if (_groupes.Count > 0)
                {
                    string nomGroupe = tabControl1.SelectedTab.Text;
                    var result = MessageBox.Show(
                        $"Êtes vous sûr de vouloir supprimer le groupe \"{nomGroupe}\" ainsi que tout son contenu ?",
                        $"Confirmer la suppression de \"{nomGroupe}\"",
                        MessageBoxButtons.YesNo
                    );

                    if (result == DialogResult.Yes)
                    {
                        bool trouve = false;
                        int i = 0;
                        while (!trouve)
                        {
                            if (_groupes[i].Nom == nomGroupe.Split(" - ")[0])
                            {
                                _groupes[i].Supprimer(null);
                                string nom = _groupes[i].Nom;
                                _groupes.RemoveAt(i);
                                tabControl1.TabPages.Remove(tabControl1.SelectedTab);
                                if (_groupesChargees.Contains(nom))
                                {
                                    _groupesChargees.Remove(nom);
                                }
                                trouve = true;
                            }
                            i++;
                        }
                        //ChargerGroupes(true);
                    }
                }
            }
        }

        /// <summary>
        /// Permet de charger les groupes
        /// </summary>
        private void ChargerGroupes()
        {
            this.tabControl1.TabPages.Clear();
            foreach (Groupe groupe in _groupes)
            {
                TabPage tabPage = new TabPage($"{groupe.Nom} - {groupe.Description}");
                tabPage.BackColor = Color.FromArgb(18, 18, 18);
                tabPage.ToolTipText = groupe.Description;
                this.tabControl1.TabPages.Add(tabPage);
                tabControl1.Enabled = true;
            }
        }

        /// <summary>
        /// Permet de charger le contenu d'un groupe
        /// </summary>
        /// <param name="groupe">Le groupe</param>
        /// <param name="newOrDeleteGroup">Si c'est dû à l'ajout d'un nouveau groupe ou d'une suppression d'un groupe</param>
        /// <param name="tabPage">L'onglet qui contiens le groupe</param>
        async private void ChargerContenuGroupe(Groupe groupe, bool newOrDeleteGroup, TabPage tabPage)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (!newOrDeleteGroup)
                {
                    await groupe.ChargerFichiers(null);
                }
            }
            catch (Exception)
            {
                var result = MessageBox.Show(
                    "FFMpeg.exe doit être installé sur votre ordinateur pour continuer. Voulez vous l'installer ?",
                    "Important",
                    MessageBoxButtons.YesNo
                    );
                if (result == DialogResult.Yes)
                {
                    tabControl1.Enabled = false;
                    await FFmpegDownloader.GetLatestVersion(FFmpegVersion.Full);
                }
                else
                {
                    this.Close();
                }
            }

            FlowLayoutPanel flowLayoutPanel = new FlowLayoutPanel();
            flowLayoutPanel.Dock = DockStyle.Fill;
            flowLayoutPanel.AutoScroll = true;
            flowLayoutPanel.Controls.Clear();
            foreach (var fichier in groupe.Fichiers)
            {
                if (fichier is Video)
                {
                    PictureBox thumbnail = new PictureBox();
                    thumbnail.SizeMode = PictureBoxSizeMode.StretchImage;
                    thumbnail.Size = new Size(235, 150);
                    thumbnail.ImageLocation = Path.ChangeExtension(fichier.Chemin, ".png");
                    thumbnail.MouseDoubleClick += (sender, e) => Thumbnail_MouseDoubleClick(sender, e, (Video)fichier);

                    ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
                    ToolStripMenuItem btnSupprimer = new ToolStripMenuItem();
                    btnSupprimer.Text = "Supprimer";
                    btnSupprimer.Font = new Font("Montserrat", 9, FontStyle.Regular);
                    contextMenuStrip.Items.Add(btnSupprimer);
                    btnSupprimer.Click += (sender, e) => BtnSupprimer_DropDownItemClicked(sender, e, groupe, fichier.Nom);

                    ToolStripMenuItem btnMettreEnLigneYoutube = new ToolStripMenuItem();
                    btnMettreEnLigneYoutube.Text = "Mettre en ligne sur YouTube";
                    btnMettreEnLigneYoutube.Font = new Font("Montserrat", 9, FontStyle.Regular);
                    contextMenuStrip.Items.Add(btnMettreEnLigneYoutube);
                    btnMettreEnLigneYoutube.Click += (sender, e) => BtnMettreEnLigneYoutube_Click(sender, e, fichier.Chemin);

                    ToolStripMenuItem btnCouper = new ToolStripMenuItem();
                    btnCouper.Text = "Couper la vidéo";
                    btnCouper.Font = new Font("Montserrat", 9, FontStyle.Regular);
                    contextMenuStrip.Items.Add(btnCouper);
                    btnCouper.Click += (sender, e) => BtnCouper_DropDownItemClicked(sender, e, groupe, fichier.Chemin);

                    thumbnail.ContextMenuStrip = contextMenuStrip;

                    FlowLayoutPanel flpPb = new FlowLayoutPanel();
                    flpPb.FlowDirection = FlowDirection.TopDown;
                    flpPb.Size = new Size(235, 200);
                    Label lblTitre = new Label();
                    lblTitre.ForeColor = Color.White;
                    lblTitre.Text = fichier.Nom + " - Image";
                    flpPb.Controls.Add(thumbnail);
                    flpPb.Controls.Add(lblTitre);
                    flowLayoutPanel.Controls.Add(flpPb);
                }
                else if (fichier is Image)
                {
                    PictureBox pbImage = new PictureBox();
                    pbImage.SizeMode = PictureBoxSizeMode.StretchImage;
                    pbImage.Size = new Size(((Image)fichier).Resolution.Width / 5, ((Image)fichier).Resolution.Height / 5);
                    pbImage.ImageLocation = fichier.Chemin;
                    pbImage.MouseDoubleClick += (sender, e) => PbImage_MouseDoubleClick(sender, e, (Image)fichier);

                    ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
                    contextMenuStrip.Font = new Font("Montserrat", 9, FontStyle.Regular);
                    ToolStripMenuItem btnSupprimer = new ToolStripMenuItem();
                    btnSupprimer.Text = "Supprimer";
                    contextMenuStrip.Items.Add(btnSupprimer);
                    btnSupprimer.Click += (sender, e) => BtnSupprimer_DropDownItemClicked(sender, e, groupe, fichier.Nom);

                    ToolStripMenuItem btnMettreEnLigne = new ToolStripMenuItem();
                    btnMettreEnLigne.Text = "Mettre en ligne sur Imgur";
                    contextMenuStrip.Items.Add(btnMettreEnLigne);
                    btnMettreEnLigne.Click += (sender, e) => BtnMettreEnLigne_Click(sender, e, (Image)fichier);

                    pbImage.ContextMenuStrip = contextMenuStrip;

                    FlowLayoutPanel flpPb = new FlowLayoutPanel();
                    flpPb.FlowDirection = FlowDirection.TopDown;
                    flpPb.Size = new Size(((Image)fichier).Resolution.Width / 5,
                        ((Image)fichier).Resolution.Height / 5 + 30);
                    Label lblTitre = new Label();
                    lblTitre.ForeColor = Color.White;
                    lblTitre.Text = fichier.Nom;
                    flpPb.Controls.Add(pbImage);
                    flpPb.Controls.Add(lblTitre);
                    flowLayoutPanel.Controls.Add(flpPb);
                }
            }

            tabPage.Controls.Add(flowLayoutPanel);
            this.Cursor = Cursors.Default;
        }

        async private void BtnMettreEnLigne_Click(object sender, EventArgs e, Image image)
        {
            await image.MettreSurImgur();
        }

        private void PbImage_MouseDoubleClick(object sender, MouseEventArgs e, Image image)
        {
            var p = new Process();
            p.StartInfo = new ProcessStartInfo(image.Chemin)
            {
                UseShellExecute = true
            };
            p.Start();
        }

        private void BtnMettreEnLigneYoutube_Click(object sender, EventArgs e, string chemin)
        {
            YoutubeForm youtubeForm = new YoutubeForm(chemin);
            youtubeForm.Show();
        }

        private void Thumbnail_MouseDoubleClick(object sender, MouseEventArgs e, Video video)
        {
            var p = new Process();
            p.StartInfo = new ProcessStartInfo(video.Chemin)
            {
                UseShellExecute = true
            };
            p.Start();
        }

        private void BtnSupprimer_DropDownItemClicked(object sender, EventArgs e, Groupe groupe, string fichier)
        {
            var result = MessageBox.Show(
                "Êtes-vous sûr de vouloir supprimer ce fichier ?",
                "Confirmation",
                MessageBoxButtons.YesNo
                );

            if (result == DialogResult.Yes)
            {
                Fichier leFichier = groupe.Fichiers.Where(e => e.Nom == fichier).First();
                leFichier.Supprimer();
                File.Delete(Path.ChangeExtension(leFichier.Chemin, ".png"));
                groupe.Fichiers.Remove(leFichier);
                tabControl1.SelectedTab.Controls.Clear();
                ChargerContenuGroupe(groupe, true, tabControl1.SelectedTab);
            }
        }

        private void BtnCouper_DropDownItemClicked(object sender, EventArgs e, Groupe groupe, string fichier)
        {
            //IMediaInfo mediaInfo = await FFmpeg.GetMediaInfo(fichier);
            //IVideoStream videoStream = mediaInfo.VideoStreams.FirstOrDefault();
            //CouperVideo couperVideo = new CouperVideo();
            //couperVideo.DureeTotale = videoStream.Duration;

            //var result = couperVideo.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    await Video.CouperVideo(videoStream, couperVideo.De, couperVideo.A);
            //}
            MessageBox.Show("À venir.");
        }

        async private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_groupes.Count > 0)
            {
                Groupe groupe = new Groupe("test", "test");
                string nomGroupe = "";
                if (tabControl1.SelectedTab == null)
                {
                    nomGroupe = _groupes[0].Nom;
                }
                else
                {
                    nomGroupe = tabControl1.SelectedTab.Text;
                }

                bool trouve = false;
                int i = 0;

                while (!trouve)
                {
                    if (_groupes[i].Nom == nomGroupe)
                    {
                        groupe = _groupes[i];
                        trouve = true;
                    }
                    i++;
                }

                while (tabControl1.SelectedTab == null)
                {
                    await Task.Delay(25);
                }

                if (!_groupesChargees.Contains(groupe.Nom))
                {
                    ChargerContenuGroupe(groupe, false, tabControl1.SelectedTab);
                    _groupesChargees.Add(groupe.Nom);
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void Medias_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
