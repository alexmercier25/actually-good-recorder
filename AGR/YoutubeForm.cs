﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace AGR
{
    public partial class YoutubeForm : Form
    {
        private string _filePath;

        public YoutubeForm(string pFilePath)
        {
            InitializeComponent();
            _filePath = pFilePath;
        }

        private void YoutubeForm_Load(object sender, EventArgs e)
        {

        }

        [STAThread]
        private void btnMettreEnLigne_Click(object sender, EventArgs e)
        {
            try
            {
                Run();
                btnMettreEnLigne.Enabled = false;
            }
            catch (AggregateException ex)
            {
                foreach (var exc in ex.InnerExceptions)
                {
                    MessageBox.Show("Erreur: " + exc.Message);
                }
            }
        }

        /// <summary>
        /// Démarre la mise en ligne de la vidéo.
        /// </summary>
        /// <returns>Une tâche asyncrone.</returns>
        private async Task Run()
        {
            UserCredential credential;
            using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    // This OAuth 2.0 access scope allows an application to upload files to the
                    // authenticated user's YouTube channel, but doesn't allow other types of access.
                    new[] { YouTubeService.Scope.YoutubeUpload },
                    "user",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            var video = new Google.Apis.YouTube.v3.Data.Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = txtTitre.Text;
            video.Snippet.Description = txtDescription.Text;
            video.Snippet.Tags = txtTags.Text.Split(", ");
            //video.Snippet.CategoryId = "22"; // See https://developers.google.com/youtube/v3/docs/videoCategories/list
            video.Status = new VideoStatus();

            switch (cbVisibilite.SelectedItem)
            {
                case "Publique":
                    video.Status.PrivacyStatus = "public";
                    break;
                case "Privée":
                    video.Status.PrivacyStatus = "private";
                    break;
                case "Non répertoriée":
                    video.Status.PrivacyStatus = "unlisted";
                    break;
                default:
                    video.Status.PrivacyStatus = "unlisted";
                    break;
            }

            var filePath = _filePath; // Replace with path to actual movie file.

            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
                videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
                videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;
                await videosInsertRequest.UploadAsync();
            }
        }

        /// <summary>
        /// Quand le progrès de mise en ligne de la vidéo change.
        /// </summary>
        /// <param name="progress">Le progrès de la vidéo.</param>
        void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    lblBytes.Text = progress.BytesSent.ToString() + " bits envoyé(s)";
                    this.Enabled = false;
                    break;

                case UploadStatus.Failed:
                    MessageBox.Show($"Une erreur est survenue.\n{progress.Exception}");
                    this.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Quand la vidéo est mise en ligne.
        /// </summary>
        /// <param name="video">La vidéo</param>
        void videosInsertRequest_ResponseReceived(Google.Apis.YouTube.v3.Data.Video video)
        {
            MessageBox.Show($"La vidéo '{video.Id}' a été mise en ligne.");
        }
    }
}
