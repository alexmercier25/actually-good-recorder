﻿namespace AGR
{
    partial class YoutubeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YoutubeForm));
            this.txtTitre = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtTags = new System.Windows.Forms.TextBox();
            this.cbVisibilite = new System.Windows.Forms.ComboBox();
            this.btnMettreEnLigne = new System.Windows.Forms.Button();
            this.lblBytes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtTitre
            // 
            this.txtTitre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtTitre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTitre.ForeColor = System.Drawing.SystemColors.Control;
            this.txtTitre.Location = new System.Drawing.Point(14, 13);
            this.txtTitre.Multiline = true;
            this.txtTitre.Name = "txtTitre";
            this.txtTitre.PlaceholderText = "Titre";
            this.txtTitre.Size = new System.Drawing.Size(886, 26);
            this.txtTitre.TabIndex = 0;
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDescription.ForeColor = System.Drawing.SystemColors.Control;
            this.txtDescription.Location = new System.Drawing.Point(14, 45);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.PlaceholderText = "Description";
            this.txtDescription.Size = new System.Drawing.Size(886, 109);
            this.txtDescription.TabIndex = 1;
            // 
            // txtTags
            // 
            this.txtTags.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.txtTags.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTags.ForeColor = System.Drawing.SystemColors.Control;
            this.txtTags.Location = new System.Drawing.Point(14, 160);
            this.txtTags.Multiline = true;
            this.txtTags.Name = "txtTags";
            this.txtTags.PlaceholderText = "Tags (séparés d\'une virgule et d\'une espace)";
            this.txtTags.Size = new System.Drawing.Size(886, 25);
            this.txtTags.TabIndex = 2;
            // 
            // cbVisibilite
            // 
            this.cbVisibilite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.cbVisibilite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVisibilite.FormattingEnabled = true;
            this.cbVisibilite.Items.AddRange(new object[] {
            "Publique",
            "Privée",
            "Non répertoriée"});
            this.cbVisibilite.Location = new System.Drawing.Point(14, 191);
            this.cbVisibilite.Name = "cbVisibilite";
            this.cbVisibilite.Size = new System.Drawing.Size(886, 24);
            this.cbVisibilite.TabIndex = 3;
            // 
            // btnMettreEnLigne
            // 
            this.btnMettreEnLigne.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnMettreEnLigne.FlatAppearance.BorderSize = 0;
            this.btnMettreEnLigne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMettreEnLigne.ForeColor = System.Drawing.SystemColors.Control;
            this.btnMettreEnLigne.Location = new System.Drawing.Point(14, 222);
            this.btnMettreEnLigne.Name = "btnMettreEnLigne";
            this.btnMettreEnLigne.Size = new System.Drawing.Size(887, 25);
            this.btnMettreEnLigne.TabIndex = 4;
            this.btnMettreEnLigne.Text = "Mettre en ligne";
            this.btnMettreEnLigne.UseVisualStyleBackColor = false;
            this.btnMettreEnLigne.Click += new System.EventHandler(this.btnMettreEnLigne_Click);
            // 
            // lblBytes
            // 
            this.lblBytes.AutoSize = true;
            this.lblBytes.Location = new System.Drawing.Point(14, 268);
            this.lblBytes.Name = "lblBytes";
            this.lblBytes.Size = new System.Drawing.Size(0, 16);
            this.lblBytes.TabIndex = 5;
            // 
            // YoutubeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.ClientSize = new System.Drawing.Size(914, 260);
            this.Controls.Add(this.lblBytes);
            this.Controls.Add(this.btnMettreEnLigne);
            this.Controls.Add(this.cbVisibilite);
            this.Controls.Add(this.txtTags);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtTitre);
            this.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "YoutubeForm";
            this.Text = "Mettre en ligne la vidéo sur YouTube";
            this.Load += new System.EventHandler(this.YoutubeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtTitre;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtTags;
        private System.Windows.Forms.ComboBox cbVisibilite;
        private System.Windows.Forms.Button btnMettreEnLigne;
        private System.Windows.Forms.Label lblBytes;
    }
}