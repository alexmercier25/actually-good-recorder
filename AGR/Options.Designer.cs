﻿namespace AGR
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabOptionsVideo = new System.Windows.Forms.TabPage();
            this.checkIsHardwareEncodingEnabled = new System.Windows.Forms.CheckBox();
            this.checkIsLowLatencyEnabled = new System.Windows.Forms.CheckBox();
            this.checkIsThrottlingDisabled = new System.Windows.Forms.CheckBox();
            this.checkIsMp4FastStartEnabled = new System.Windows.Forms.CheckBox();
            this.lblProfilEncodeur = new System.Windows.Forms.Label();
            this.cbProfilEncodeur = new System.Windows.Forms.ComboBox();
            this.checkIPSFixe = new System.Windows.Forms.CheckBox();
            this.numIPS = new System.Windows.Forms.NumericUpDown();
            this.lblIPS = new System.Windows.Forms.Label();
            this.numDebit = new System.Windows.Forms.NumericUpDown();
            this.lblDebit = new System.Windows.Forms.Label();
            this.cbModeDebit = new System.Windows.Forms.ComboBox();
            this.lblModeDebit = new System.Windows.Forms.Label();
            this.tabOptionsAudio = new System.Windows.Forms.TabPage();
            this.checkMicro = new System.Windows.Forms.CheckBox();
            this.cbPeripherique = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkAudioActive = new System.Windows.Forms.CheckBox();
            this.cbCannaux = new System.Windows.Forms.ComboBox();
            this.lblCanaux = new System.Windows.Forms.Label();
            this.cbDebitAudio = new System.Windows.Forms.ComboBox();
            this.lblDebitAudio = new System.Windows.Forms.Label();
            this.tabOptionsSouris = new System.Windows.Forms.TabPage();
            this.cbMouseClickDetectionMode = new System.Windows.Forms.ComboBox();
            this.lblModeDetectionClicks = new System.Windows.Forms.Label();
            this.checkIsMousePointerEnabled = new System.Windows.Forms.CheckBox();
            this.numMouseClickDetectionDuration = new System.Windows.Forms.NumericUpDown();
            this.lblDuree = new System.Windows.Forms.Label();
            this.numMouseClickDetectionRadius = new System.Windows.Forms.NumericUpDown();
            this.lblTailleRayon = new System.Windows.Forms.Label();
            this.lblMouseRightClickDetectionColor = new System.Windows.Forms.Label();
            this.btnMouseRightClickDetectionColor = new System.Windows.Forms.Button();
            this.lblMouseClickDetectionColor = new System.Windows.Forms.Label();
            this.btnMouseClickDetectionColor = new System.Windows.Forms.Button();
            this.checkIsMouseClicksDetected = new System.Windows.Forms.CheckBox();
            this.tabPreferences = new System.Windows.Forms.TabPage();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnAppliquer = new System.Windows.Forms.Button();
            this.btnParDefaut = new System.Windows.Forms.Button();
            this.colorDialogClics = new System.Windows.Forms.ColorDialog();
            this.tabControl1.SuspendLayout();
            this.tabOptionsVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDebit)).BeginInit();
            this.tabOptionsAudio.SuspendLayout();
            this.tabOptionsSouris.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMouseClickDetectionDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMouseClickDetectionRadius)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabOptionsVideo);
            this.tabControl1.Controls.Add(this.tabOptionsAudio);
            this.tabControl1.Controls.Add(this.tabOptionsSouris);
            this.tabControl1.Controls.Add(this.tabPreferences);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 3;
            this.tabControl1.Size = new System.Drawing.Size(587, 493);
            this.tabControl1.TabIndex = 0;
            // 
            // tabOptionsVideo
            // 
            this.tabOptionsVideo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.tabOptionsVideo.Controls.Add(this.checkIsHardwareEncodingEnabled);
            this.tabOptionsVideo.Controls.Add(this.checkIsLowLatencyEnabled);
            this.tabOptionsVideo.Controls.Add(this.checkIsThrottlingDisabled);
            this.tabOptionsVideo.Controls.Add(this.checkIsMp4FastStartEnabled);
            this.tabOptionsVideo.Controls.Add(this.lblProfilEncodeur);
            this.tabOptionsVideo.Controls.Add(this.cbProfilEncodeur);
            this.tabOptionsVideo.Controls.Add(this.checkIPSFixe);
            this.tabOptionsVideo.Controls.Add(this.numIPS);
            this.tabOptionsVideo.Controls.Add(this.lblIPS);
            this.tabOptionsVideo.Controls.Add(this.numDebit);
            this.tabOptionsVideo.Controls.Add(this.lblDebit);
            this.tabOptionsVideo.Controls.Add(this.cbModeDebit);
            this.tabOptionsVideo.Controls.Add(this.lblModeDebit);
            this.tabOptionsVideo.Location = new System.Drawing.Point(4, 28);
            this.tabOptionsVideo.Name = "tabOptionsVideo";
            this.tabOptionsVideo.Padding = new System.Windows.Forms.Padding(3);
            this.tabOptionsVideo.Size = new System.Drawing.Size(579, 461);
            this.tabOptionsVideo.TabIndex = 0;
            this.tabOptionsVideo.Text = "Vidéo";
            // 
            // checkIsHardwareEncodingEnabled
            // 
            this.checkIsHardwareEncodingEnabled.AutoSize = true;
            this.checkIsHardwareEncodingEnabled.Location = new System.Drawing.Point(9, 252);
            this.checkIsHardwareEncodingEnabled.Name = "checkIsHardwareEncodingEnabled";
            this.checkIsHardwareEncodingEnabled.Size = new System.Drawing.Size(140, 20);
            this.checkIsHardwareEncodingEnabled.TabIndex = 1;
            this.checkIsHardwareEncodingEnabled.Text = "Encodage matériel";
            this.checkIsHardwareEncodingEnabled.UseVisualStyleBackColor = true;
            // 
            // checkIsLowLatencyEnabled
            // 
            this.checkIsLowLatencyEnabled.AutoSize = true;
            this.checkIsLowLatencyEnabled.Location = new System.Drawing.Point(188, 226);
            this.checkIsLowLatencyEnabled.Name = "checkIsLowLatencyEnabled";
            this.checkIsLowLatencyEnabled.Size = new System.Drawing.Size(109, 20);
            this.checkIsLowLatencyEnabled.TabIndex = 2;
            this.checkIsLowLatencyEnabled.Text = "Faible latence";
            this.checkIsLowLatencyEnabled.UseVisualStyleBackColor = true;
            // 
            // checkIsThrottlingDisabled
            // 
            this.checkIsThrottlingDisabled.AutoSize = true;
            this.checkIsThrottlingDisabled.Location = new System.Drawing.Point(9, 226);
            this.checkIsThrottlingDisabled.Name = "checkIsThrottlingDisabled";
            this.checkIsThrottlingDisabled.Size = new System.Drawing.Size(154, 20);
            this.checkIsThrottlingDisabled.TabIndex = 0;
            this.checkIsThrottlingDisabled.Text = "\"Throttling\" désactivé";
            this.checkIsThrottlingDisabled.UseVisualStyleBackColor = true;
            // 
            // checkIsMp4FastStartEnabled
            // 
            this.checkIsMp4FastStartEnabled.AutoSize = true;
            this.checkIsMp4FastStartEnabled.Location = new System.Drawing.Point(188, 252);
            this.checkIsMp4FastStartEnabled.Name = "checkIsMp4FastStartEnabled";
            this.checkIsMp4FastStartEnabled.Size = new System.Drawing.Size(167, 20);
            this.checkIsMp4FastStartEnabled.TabIndex = 3;
            this.checkIsMp4FastStartEnabled.Text = "Démarrage rapide MP4";
            this.checkIsMp4FastStartEnabled.UseVisualStyleBackColor = true;
            // 
            // lblProfilEncodeur
            // 
            this.lblProfilEncodeur.AutoSize = true;
            this.lblProfilEncodeur.Location = new System.Drawing.Point(9, 130);
            this.lblProfilEncodeur.Name = "lblProfilEncodeur";
            this.lblProfilEncodeur.Size = new System.Drawing.Size(133, 16);
            this.lblProfilEncodeur.TabIndex = 8;
            this.lblProfilEncodeur.Text = "Profil de l\'encodeur : ";
            // 
            // cbProfilEncodeur
            // 
            this.cbProfilEncodeur.FormattingEnabled = true;
            this.cbProfilEncodeur.Items.AddRange(new object[] {
            "Main",
            "Baseline",
            "High"});
            this.cbProfilEncodeur.Location = new System.Drawing.Point(165, 130);
            this.cbProfilEncodeur.Name = "cbProfilEncodeur";
            this.cbProfilEncodeur.Size = new System.Drawing.Size(405, 24);
            this.cbProfilEncodeur.TabIndex = 7;
            // 
            // checkIPSFixe
            // 
            this.checkIPSFixe.AutoSize = true;
            this.checkIPSFixe.Location = new System.Drawing.Point(165, 105);
            this.checkIPSFixe.Name = "checkIPSFixe";
            this.checkIPSFixe.Size = new System.Drawing.Size(235, 20);
            this.checkIPSFixe.TabIndex = 6;
            this.checkIPSFixe.Text = "Nombre d\'images par seconde fixé";
            this.checkIPSFixe.UseVisualStyleBackColor = true;
            // 
            // numIPS
            // 
            this.numIPS.Location = new System.Drawing.Point(165, 73);
            this.numIPS.Name = "numIPS";
            this.numIPS.Size = new System.Drawing.Size(405, 22);
            this.numIPS.TabIndex = 5;
            // 
            // lblIPS
            // 
            this.lblIPS.AutoSize = true;
            this.lblIPS.Location = new System.Drawing.Point(9, 73);
            this.lblIPS.Name = "lblIPS";
            this.lblIPS.Size = new System.Drawing.Size(135, 16);
            this.lblIPS.TabIndex = 4;
            this.lblIPS.Text = "Images par seconde :";
            // 
            // numDebit
            // 
            this.numDebit.Location = new System.Drawing.Point(165, 42);
            this.numDebit.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numDebit.Name = "numDebit";
            this.numDebit.Size = new System.Drawing.Size(405, 22);
            this.numDebit.TabIndex = 3;
            // 
            // lblDebit
            // 
            this.lblDebit.AutoSize = true;
            this.lblDebit.Location = new System.Drawing.Point(97, 44);
            this.lblDebit.Name = "lblDebit";
            this.lblDebit.Size = new System.Drawing.Size(46, 16);
            this.lblDebit.TabIndex = 2;
            this.lblDebit.Text = "Débit :";
            // 
            // cbModeDebit
            // 
            this.cbModeDebit.FormattingEnabled = true;
            this.cbModeDebit.Items.AddRange(new object[] {
            "UnconstrainedVBR",
            "CBR",
            "Quality"});
            this.cbModeDebit.Location = new System.Drawing.Point(165, 11);
            this.cbModeDebit.Name = "cbModeDebit";
            this.cbModeDebit.Size = new System.Drawing.Size(405, 24);
            this.cbModeDebit.TabIndex = 1;
            // 
            // lblModeDebit
            // 
            this.lblModeDebit.AutoSize = true;
            this.lblModeDebit.Location = new System.Drawing.Point(41, 11);
            this.lblModeDebit.Name = "lblModeDebit";
            this.lblModeDebit.Size = new System.Drawing.Size(99, 16);
            this.lblModeDebit.TabIndex = 0;
            this.lblModeDebit.Text = "Mode de débit :";
            // 
            // tabOptionsAudio
            // 
            this.tabOptionsAudio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.tabOptionsAudio.Controls.Add(this.checkMicro);
            this.tabOptionsAudio.Controls.Add(this.cbPeripherique);
            this.tabOptionsAudio.Controls.Add(this.label1);
            this.tabOptionsAudio.Controls.Add(this.checkAudioActive);
            this.tabOptionsAudio.Controls.Add(this.cbCannaux);
            this.tabOptionsAudio.Controls.Add(this.lblCanaux);
            this.tabOptionsAudio.Controls.Add(this.cbDebitAudio);
            this.tabOptionsAudio.Controls.Add(this.lblDebitAudio);
            this.tabOptionsAudio.Location = new System.Drawing.Point(4, 28);
            this.tabOptionsAudio.Name = "tabOptionsAudio";
            this.tabOptionsAudio.Padding = new System.Windows.Forms.Padding(3);
            this.tabOptionsAudio.Size = new System.Drawing.Size(579, 461);
            this.tabOptionsAudio.TabIndex = 1;
            this.tabOptionsAudio.Text = "Audio";
            // 
            // checkMicro
            // 
            this.checkMicro.AutoSize = true;
            this.checkMicro.Location = new System.Drawing.Point(101, 132);
            this.checkMicro.Name = "checkMicro";
            this.checkMicro.Size = new System.Drawing.Size(257, 20);
            this.checkMicro.TabIndex = 7;
            this.checkMicro.Text = "Enregistrement du microphone activé";
            this.checkMicro.UseVisualStyleBackColor = true;
            // 
            // cbPeripherique
            // 
            this.cbPeripherique.FormattingEnabled = true;
            this.cbPeripherique.Location = new System.Drawing.Point(101, 101);
            this.cbPeripherique.Name = "cbPeripherique";
            this.cbPeripherique.Size = new System.Drawing.Size(468, 24);
            this.cbPeripherique.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Périphérique : ";
            // 
            // checkAudioActive
            // 
            this.checkAudioActive.AutoSize = true;
            this.checkAudioActive.Location = new System.Drawing.Point(101, 13);
            this.checkAudioActive.Name = "checkAudioActive";
            this.checkAudioActive.Size = new System.Drawing.Size(101, 20);
            this.checkAudioActive.TabIndex = 4;
            this.checkAudioActive.Text = "Audio activé";
            this.checkAudioActive.UseVisualStyleBackColor = true;
            this.checkAudioActive.CheckedChanged += new System.EventHandler(this.checkAudioActive_CheckedChanged);
            // 
            // cbCannaux
            // 
            this.cbCannaux.FormattingEnabled = true;
            this.cbCannaux.Items.AddRange(new object[] {
            "5.1",
            "Mono",
            "Stereo"});
            this.cbCannaux.Location = new System.Drawing.Point(101, 70);
            this.cbCannaux.Name = "cbCannaux";
            this.cbCannaux.Size = new System.Drawing.Size(468, 24);
            this.cbCannaux.TabIndex = 3;
            // 
            // lblCanaux
            // 
            this.lblCanaux.AutoSize = true;
            this.lblCanaux.Location = new System.Drawing.Point(25, 74);
            this.lblCanaux.Name = "lblCanaux";
            this.lblCanaux.Size = new System.Drawing.Size(66, 16);
            this.lblCanaux.TabIndex = 2;
            this.lblCanaux.Text = "Cannaux :";
            // 
            // cbDebitAudio
            // 
            this.cbDebitAudio.FormattingEnabled = true;
            this.cbDebitAudio.Items.AddRange(new object[] {
            "96",
            "128",
            "160",
            "192"});
            this.cbDebitAudio.Location = new System.Drawing.Point(101, 39);
            this.cbDebitAudio.Name = "cbDebitAudio";
            this.cbDebitAudio.Size = new System.Drawing.Size(468, 24);
            this.cbDebitAudio.TabIndex = 1;
            // 
            // lblDebitAudio
            // 
            this.lblDebitAudio.AutoSize = true;
            this.lblDebitAudio.Location = new System.Drawing.Point(9, 39);
            this.lblDebitAudio.Name = "lblDebitAudio";
            this.lblDebitAudio.Size = new System.Drawing.Size(83, 16);
            this.lblDebitAudio.TabIndex = 0;
            this.lblDebitAudio.Text = "Débit audio :";
            // 
            // tabOptionsSouris
            // 
            this.tabOptionsSouris.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.tabOptionsSouris.Controls.Add(this.cbMouseClickDetectionMode);
            this.tabOptionsSouris.Controls.Add(this.lblModeDetectionClicks);
            this.tabOptionsSouris.Controls.Add(this.checkIsMousePointerEnabled);
            this.tabOptionsSouris.Controls.Add(this.numMouseClickDetectionDuration);
            this.tabOptionsSouris.Controls.Add(this.lblDuree);
            this.tabOptionsSouris.Controls.Add(this.numMouseClickDetectionRadius);
            this.tabOptionsSouris.Controls.Add(this.lblTailleRayon);
            this.tabOptionsSouris.Controls.Add(this.lblMouseRightClickDetectionColor);
            this.tabOptionsSouris.Controls.Add(this.btnMouseRightClickDetectionColor);
            this.tabOptionsSouris.Controls.Add(this.lblMouseClickDetectionColor);
            this.tabOptionsSouris.Controls.Add(this.btnMouseClickDetectionColor);
            this.tabOptionsSouris.Controls.Add(this.checkIsMouseClicksDetected);
            this.tabOptionsSouris.Location = new System.Drawing.Point(4, 28);
            this.tabOptionsSouris.Name = "tabOptionsSouris";
            this.tabOptionsSouris.Size = new System.Drawing.Size(579, 461);
            this.tabOptionsSouris.TabIndex = 2;
            this.tabOptionsSouris.Text = "Souris";
            // 
            // cbMouseClickDetectionMode
            // 
            this.cbMouseClickDetectionMode.FormattingEnabled = true;
            this.cbMouseClickDetectionMode.Items.AddRange(new object[] {
            "Hook",
            "Polling"});
            this.cbMouseClickDetectionMode.Location = new System.Drawing.Point(202, 215);
            this.cbMouseClickDetectionMode.Name = "cbMouseClickDetectionMode";
            this.cbMouseClickDetectionMode.Size = new System.Drawing.Size(366, 24);
            this.cbMouseClickDetectionMode.TabIndex = 9;
            // 
            // lblModeDetectionClicks
            // 
            this.lblModeDetectionClicks.AutoSize = true;
            this.lblModeDetectionClicks.Location = new System.Drawing.Point(9, 219);
            this.lblModeDetectionClicks.Name = "lblModeDetectionClicks";
            this.lblModeDetectionClicks.Size = new System.Drawing.Size(182, 16);
            this.lblModeDetectionClicks.TabIndex = 8;
            this.lblModeDetectionClicks.Text = "Mode de détection des clics : ";
            // 
            // checkIsMousePointerEnabled
            // 
            this.checkIsMousePointerEnabled.AutoSize = true;
            this.checkIsMousePointerEnabled.Location = new System.Drawing.Point(9, 183);
            this.checkIsMousePointerEnabled.Name = "checkIsMousePointerEnabled";
            this.checkIsMousePointerEnabled.Size = new System.Drawing.Size(228, 20);
            this.checkIsMousePointerEnabled.TabIndex = 7;
            this.checkIsMousePointerEnabled.Text = "Affichage du pointeur de la souris";
            this.checkIsMousePointerEnabled.UseVisualStyleBackColor = true;
            // 
            // numMouseClickDetectionDuration
            // 
            this.numMouseClickDetectionDuration.Location = new System.Drawing.Point(118, 146);
            this.numMouseClickDetectionDuration.Name = "numMouseClickDetectionDuration";
            this.numMouseClickDetectionDuration.Size = new System.Drawing.Size(451, 22);
            this.numMouseClickDetectionDuration.TabIndex = 6;
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.Location = new System.Drawing.Point(26, 148);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(83, 16);
            this.lblDuree.TabIndex = 5;
            this.lblDuree.Text = "Durée (ms) : ";
            // 
            // numMouseClickDetectionRadius
            // 
            this.numMouseClickDetectionRadius.Location = new System.Drawing.Point(118, 108);
            this.numMouseClickDetectionRadius.Name = "numMouseClickDetectionRadius";
            this.numMouseClickDetectionRadius.Size = new System.Drawing.Size(451, 22);
            this.numMouseClickDetectionRadius.TabIndex = 4;
            // 
            // lblTailleRayon
            // 
            this.lblTailleRayon.AutoSize = true;
            this.lblTailleRayon.Location = new System.Drawing.Point(9, 110);
            this.lblTailleRayon.Name = "lblTailleRayon";
            this.lblTailleRayon.Size = new System.Drawing.Size(100, 16);
            this.lblTailleRayon.TabIndex = 3;
            this.lblTailleRayon.Text = "Taille du rayon :";
            // 
            // lblMouseRightClickDetectionColor
            // 
            this.lblMouseRightClickDetectionColor.AutoSize = true;
            this.lblMouseRightClickDetectionColor.Location = new System.Drawing.Point(53, 80);
            this.lblMouseRightClickDetectionColor.Name = "lblMouseRightClickDetectionColor";
            this.lblMouseRightClickDetectionColor.Size = new System.Drawing.Size(238, 16);
            this.lblMouseRightClickDetectionColor.TabIndex = 2;
            this.lblMouseRightClickDetectionColor.Text = "Couleur de la détection des clics droits";
            // 
            // btnMouseRightClickDetectionColor
            // 
            this.btnMouseRightClickDetectionColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnMouseRightClickDetectionColor.FlatAppearance.BorderSize = 0;
            this.btnMouseRightClickDetectionColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMouseRightClickDetectionColor.Location = new System.Drawing.Point(9, 70);
            this.btnMouseRightClickDetectionColor.Name = "btnMouseRightClickDetectionColor";
            this.btnMouseRightClickDetectionColor.Size = new System.Drawing.Size(37, 34);
            this.btnMouseRightClickDetectionColor.TabIndex = 1;
            this.btnMouseRightClickDetectionColor.UseVisualStyleBackColor = false;
            this.btnMouseRightClickDetectionColor.Click += new System.EventHandler(this.btnMouseRightClickDetectionColor_Click);
            // 
            // lblMouseClickDetectionColor
            // 
            this.lblMouseClickDetectionColor.AutoSize = true;
            this.lblMouseClickDetectionColor.Location = new System.Drawing.Point(53, 39);
            this.lblMouseClickDetectionColor.Name = "lblMouseClickDetectionColor";
            this.lblMouseClickDetectionColor.Size = new System.Drawing.Size(200, 16);
            this.lblMouseClickDetectionColor.TabIndex = 2;
            this.lblMouseClickDetectionColor.Text = "Couleur de la détection des clics";
            // 
            // btnMouseClickDetectionColor
            // 
            this.btnMouseClickDetectionColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnMouseClickDetectionColor.FlatAppearance.BorderSize = 0;
            this.btnMouseClickDetectionColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMouseClickDetectionColor.Location = new System.Drawing.Point(9, 30);
            this.btnMouseClickDetectionColor.Name = "btnMouseClickDetectionColor";
            this.btnMouseClickDetectionColor.Size = new System.Drawing.Size(37, 34);
            this.btnMouseClickDetectionColor.TabIndex = 1;
            this.btnMouseClickDetectionColor.UseVisualStyleBackColor = false;
            this.btnMouseClickDetectionColor.Click += new System.EventHandler(this.btnMouseClickDetectionColor_Click);
            // 
            // checkIsMouseClicksDetected
            // 
            this.checkIsMouseClicksDetected.AutoSize = true;
            this.checkIsMouseClicksDetected.Location = new System.Drawing.Point(9, 3);
            this.checkIsMouseClicksDetected.Name = "checkIsMouseClicksDetected";
            this.checkIsMouseClicksDetected.Size = new System.Drawing.Size(196, 20);
            this.checkIsMouseClicksDetected.TabIndex = 0;
            this.checkIsMouseClicksDetected.Text = "Activer la détection des clics";
            this.checkIsMouseClicksDetected.UseVisualStyleBackColor = true;
            this.checkIsMouseClicksDetected.CheckedChanged += new System.EventHandler(this.checkIsMouseClicksDetected_CheckedChanged);
            // 
            // tabPreferences
            // 
            this.tabPreferences.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.tabPreferences.Location = new System.Drawing.Point(4, 28);
            this.tabPreferences.Margin = new System.Windows.Forms.Padding(0);
            this.tabPreferences.Name = "tabPreferences";
            this.tabPreferences.Padding = new System.Windows.Forms.Padding(3);
            this.tabPreferences.Size = new System.Drawing.Size(579, 461);
            this.tabPreferences.TabIndex = 3;
            this.tabPreferences.Text = "Préférences";
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnAnnuler.FlatAppearance.BorderSize = 0;
            this.btnAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnuler.Location = new System.Drawing.Point(488, 499);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(86, 25);
            this.btnAnnuler.TabIndex = 1;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnAppliquer
            // 
            this.btnAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnAppliquer.FlatAppearance.BorderSize = 0;
            this.btnAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAppliquer.Location = new System.Drawing.Point(192, 499);
            this.btnAppliquer.Name = "btnAppliquer";
            this.btnAppliquer.Size = new System.Drawing.Size(289, 25);
            this.btnAppliquer.TabIndex = 2;
            this.btnAppliquer.Text = "Appliquer les modifications et quitter";
            this.btnAppliquer.UseVisualStyleBackColor = false;
            this.btnAppliquer.Click += new System.EventHandler(this.btnAppliquer_Click);
            // 
            // btnParDefaut
            // 
            this.btnParDefaut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.btnParDefaut.Enabled = false;
            this.btnParDefaut.FlatAppearance.BorderSize = 0;
            this.btnParDefaut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParDefaut.Location = new System.Drawing.Point(8, 499);
            this.btnParDefaut.Name = "btnParDefaut";
            this.btnParDefaut.Size = new System.Drawing.Size(86, 25);
            this.btnParDefaut.TabIndex = 3;
            this.btnParDefaut.Text = "Par défaut";
            this.btnParDefaut.UseVisualStyleBackColor = false;
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.ClientSize = new System.Drawing.Size(587, 535);
            this.Controls.Add(this.btnParDefaut);
            this.Controls.Add(this.btnAppliquer);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Montserrat", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Options";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Options_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabOptionsVideo.ResumeLayout(false);
            this.tabOptionsVideo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDebit)).EndInit();
            this.tabOptionsAudio.ResumeLayout(false);
            this.tabOptionsAudio.PerformLayout();
            this.tabOptionsSouris.ResumeLayout(false);
            this.tabOptionsSouris.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMouseClickDetectionDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMouseClickDetectionRadius)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabOptionsVideo;
        private System.Windows.Forms.TabPage tabOptionsAudio;
        private System.Windows.Forms.ComboBox cbModeDebit;
        private System.Windows.Forms.Label lblModeDebit;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnAppliquer;
        private System.Windows.Forms.Label lblIPS;
        private System.Windows.Forms.NumericUpDown numDebit;
        private System.Windows.Forms.Label lblDebit;
        private System.Windows.Forms.NumericUpDown numIPS;
        private System.Windows.Forms.ComboBox cbProfilEncodeur;
        private System.Windows.Forms.CheckBox checkIPSFixe;
        private System.Windows.Forms.Label lblProfilEncodeur;
        private System.Windows.Forms.CheckBox checkIsMp4FastStartEnabled;
        private System.Windows.Forms.CheckBox checkIsLowLatencyEnabled;
        private System.Windows.Forms.CheckBox checkIsHardwareEncodingEnabled;
        private System.Windows.Forms.CheckBox checkIsThrottlingDisabled;
        private System.Windows.Forms.Button btnParDefaut;
        private System.Windows.Forms.ComboBox cbDebitAudio;
        private System.Windows.Forms.Label lblDebitAudio;
        private System.Windows.Forms.ComboBox cbCannaux;
        private System.Windows.Forms.Label lblCanaux;
        private System.Windows.Forms.CheckBox checkAudioActive;
        private System.Windows.Forms.TabPage tabOptionsSouris;
        private System.Windows.Forms.CheckBox checkIsMouseClicksDetected;
        private System.Windows.Forms.ComboBox cbMouseClickDetectionMode;
        private System.Windows.Forms.Label lblModeDetectionClicks;
        private System.Windows.Forms.CheckBox checkIsMousePointerEnabled;
        private System.Windows.Forms.NumericUpDown numMouseClickDetectionDuration;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.NumericUpDown numMouseClickDetectionRadius;
        private System.Windows.Forms.Label lblTailleRayon;
        private System.Windows.Forms.Label lblMouseRightClickDetectionColor;
        private System.Windows.Forms.Button btnMouseRightClickDetectionColor;
        private System.Windows.Forms.Label lblMouseClickDetectionColor;
        private System.Windows.Forms.Button btnMouseClickDetectionColor;
        private System.Windows.Forms.ColorDialog colorDialogClics;
        private System.Windows.Forms.ComboBox cbPeripherique;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkMicro;
        private System.Windows.Forms.TabPage tabPreferences;
    }
}