﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AGR.Classes;

namespace AGR
{
    public partial class AjouterGroupe : Form
    {
        private Groupe _groupe;

        public Groupe Groupe
        {
            get
            {
                return _groupe;
            }
            set
            {
                _groupe = value;
            }
        }

        public AjouterGroupe()
        {
            InitializeComponent();
            btnAjouter.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            if (txtNom.Text != "")
            {
                Groupe = new Groupe(txtNom.Text, txtDescription.Text);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillez donner un nom.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
