﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Resources;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ScreenRecorderLib;
using System.Linq;
using Google.Apis.Auth.OAuth2;

namespace AGR
{
    public partial class Options : Form
    {
        private string _fichierOptions;
        private string _fichierOptionsDefaut;
        private JObject _optJson;
        private JObject _optDefautJson;
        private RecorderOptions _optionsSortie;
        private Dictionary<string, string> inputDevices;

        public RecorderOptions OptionsSortie
        {
            get { return _optionsSortie; }
            set { _optionsSortie = value; }
        }

        /// <summary>
        /// Initialise le Form et charge le fichier de paramètres.
        /// </summary>
        public Options()
        {
            InitializeComponent();

            btnAppliquer.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;

            try
            {
                StreamReader reader = new StreamReader(Path.Combine(Environment.CurrentDirectory, @"Resources", "settings.json"));
                _fichierOptions = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception e)
            {
                _fichierOptions = Resource1.defaultSettings;
            }
            _fichierOptionsDefaut = Resource1.defaultSettings;
            _optJson = JObject.Parse(_fichierOptions);
            _optDefautJson = JObject.Parse(_fichierOptionsDefaut);
        }

        #region Evenements

        private void Options_Load(object sender, EventArgs e)
        {
            ChargerOptions(_optJson);

            inputDevices = Recorder.GetSystemAudioDevices(AudioDeviceSource.InputDevices);
            foreach (var item in inputDevices)
            {
                cbPeripherique.Items.Add(item.Value);
            }
            //cbPeripherique.SelectedIndex = 0;
        }


        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkAudioActive_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkAudioActive.Checked)
            {
                cbDebitAudio.Enabled = false;
                cbCannaux.Enabled = false;
            }
            else
            {
                cbDebitAudio.Enabled = true;
                cbCannaux.Enabled = true;
            }
        }

        private void checkIsMouseClicksDetected_CheckedChanged(object sender, EventArgs e)
        {
            if (checkIsMouseClicksDetected.Checked)
            {
                btnMouseClickDetectionColor.Enabled = true;
                btnMouseRightClickDetectionColor.Enabled = true;
            }
            else
            {
                btnMouseClickDetectionColor.Enabled = false;
                btnMouseRightClickDetectionColor.Enabled = false;
            }
        }

        private void btnMouseClickDetectionColor_Click(object sender, EventArgs e)
        {
            colorDialogClics.Color = btnMouseClickDetectionColor.BackColor;
            DialogResult result = colorDialogClics.ShowDialog();
            if (result == DialogResult.OK)
            {
                btnMouseClickDetectionColor.BackColor = colorDialogClics.Color;
            }
        }

        private void btnMouseRightClickDetectionColor_Click(object sender, EventArgs e)
        {
            colorDialogClics.Color = btnMouseRightClickDetectionColor.BackColor;
            DialogResult result = colorDialogClics.ShowDialog();
            if (result == DialogResult.OK)
            {
                btnMouseRightClickDetectionColor.BackColor = colorDialogClics.Color;
            }
        }

        private void btnAppliquer_Click(object sender, EventArgs e)
        {
            SauvegarderOptions();
        }

        #endregion

        #region Methodes

        /// <summary>
        /// Chargement des options de l'enregistreur à partir d'un objet JSON
        /// </summary>
        /// <param name="options">L'objet JSON à charger</param>
        private void ChargerOptions(JObject options)
        {
            checkIsThrottlingDisabled.Checked = (bool)options["IsThrottlingDisabled"];
            checkIsHardwareEncodingEnabled.Checked = (bool)options["IsHardwareEncodingEnabled"];
            checkIsLowLatencyEnabled.Checked = (bool)options["IsLowLatencyEnabled"];
            checkIsMp4FastStartEnabled.Checked = (bool)options["IsMp4FastStartEnabled"];

            cbDebitAudio.SelectedItem = (string)options["AudioOptions"]["Bitrate"];
            cbCannaux.SelectedItem = (string)options["AudioOptions"]["Channels"];
            checkAudioActive.Checked = (bool)options["AudioOptions"]["IsAudioEnabled"];

            if (!checkAudioActive.Checked)
            {
                cbDebitAudio.Enabled = false;
                cbCannaux.Enabled = false;
            }

            cbModeDebit.SelectedItem = (string)options["VideoOptions"]["BitrateMode"];
            checkMicro.Checked = (bool) options["AudioOptions"]["IsInputDeviceEnabled"];
            string cbPerValue = "";
            if ((string) options["AudioOptions"]["AudioInputDevice"] != null)
            {
                cbPerValue = (string) options["AudioOptions"]["AudioInputDevice"];
            }
            cbPeripherique.SelectedItem = cbPerValue;
            numDebit.Value = (int)options["VideoOptions"]["Bitrate"];
            numIPS.Value = (int)options["VideoOptions"]["Framerate"];
            checkIPSFixe.Checked = (bool)options["VideoOptions"]["IsFixedFramerate"];
            cbProfilEncodeur.SelectedItem = (string)options["VideoOptions"]["EncoderProfile"];

            checkIsMouseClicksDetected.Checked = (bool) options["MouseOptions"]["IsMouseClicksDetected"];
            // TODO: couleurs
            numMouseClickDetectionRadius.Value = (int)options["MouseOptions"]["MouseClickDetectionRadius"];
            numMouseClickDetectionDuration.Value = (int)options["MouseOptions"]["MouseClickDetectionDuration"];
            checkIsMousePointerEnabled.Checked = (bool)options["MouseOptions"]["IsMousePointerEnabled"];
            cbMouseClickDetectionMode.SelectedItem = (string)options["MouseOptions"]["MouseClickDetectionMode"];
            btnMouseClickDetectionColor.BackColor =
                ColorTranslator.FromHtml((string)options["MouseOptions"]["MouseClickDetectionColor"]);
            btnMouseRightClickDetectionColor.BackColor =
                ColorTranslator.FromHtml((string)options["MouseOptions"]["MouseRightClickDetectionColor"]);
        }

        /// <summary>
        /// Permet de sauvegarder les options et de les écrire dans le fichier des paramètres.
        /// Ensuite, ça met à jour les options de l'enregistreur.
        /// </summary>
        private void SauvegarderOptions()
        {
            StreamWriter writer = new StreamWriter(Path.Combine(Environment.CurrentDirectory, @"Resources\", "settings.json"), false);
            string inputId = (string)cbPeripherique.SelectedItem;
            foreach (var item in inputDevices)
            {
                if (item.Value == (string)cbPeripherique.SelectedItem)
                {
                    inputId = item.Key;
                }
            }
            var donneesJson = new
            {
                RecorderMode = "Video",
                IsThrottlingDisabled = checkIsThrottlingDisabled.Checked,
                IsHardwareEncodingEnabled = checkIsHardwareEncodingEnabled.Checked,
                IsLowLatencyEnabled = checkIsLowLatencyEnabled.Checked,
                IsMp4FastStartEnabled = checkIsMp4FastStartEnabled.Checked,
                AudioOptions = new
                {
                    Bitrate = cbDebitAudio.SelectedItem,
                    Channels = cbCannaux.SelectedItem,
                    IsAudioEnabled = checkAudioActive.Checked,
                    IsInputDeviceEnabled = checkMicro.Checked,
                    AudioInputDevice = inputId
                },
                VideoOptions = new
                {
                    BitrateMode = cbModeDebit.SelectedItem,
                    Bitrate = numDebit.Value,
                    Framerate = numIPS.Value,
                    IsFixedFramerate = checkIPSFixe.Checked,
                    EncoderProfile = cbProfilEncodeur.SelectedItem
                },
                MouseOptions = new
                {
                    IsMouseClicksDetected = checkIsMouseClicksDetected.Checked,
                    MouseClickDetectionColor = EnHex(btnMouseClickDetectionColor.BackColor),
                    MouseRightClickDetectionColor = EnHex(btnMouseRightClickDetectionColor.BackColor),
                    MouseClickDetectionRadius = numMouseClickDetectionRadius.Value,
                    MouseClickDetectionDuration = numMouseClickDetectionDuration.Value,
                    IsMousePointerEnabled = checkIsMousePointerEnabled.Checked,
                    MouseClickDetectionMode = cbMouseClickDetectionMode.SelectedItem
                },
                Preferences = new
                {
                    cheminVideos = "Défaut"
                }
            };

            string chaineJson = JsonConvert.SerializeObject(donneesJson);
            writer.Write(chaineJson);

            writer.Close();
            this.Close();

            OptionsSortie = JsonAObjetOptions(chaineJson);
        }

        /// <summary>
        /// Permet de mettre à jour les options de l'enregistreur selon une chaîne de caractère
        /// qui représente un objet JSON.
        /// </summary>
        /// <param name="jsonString">La chaîne de caractère (JSON) à convertir</param>
        /// <returns>Les options de l'enregistreur</returns>
        public static RecorderOptions JsonAObjetOptions(string jsonString)
        {
            JObject j = JsonConvert.DeserializeObject<JObject>(jsonString);
            AudioBitrate debitAudio = AudioBitrate.bitrate_128kbps;

            switch ((string)j["AudioOptions"]["Bitrate"])
            {
                case "96 kbps":
                    debitAudio = AudioBitrate.bitrate_96kbps;
                    break;
                case "128 kbps":
                    debitAudio = AudioBitrate.bitrate_128kbps;
                    break;
                case "160 kbps":
                    debitAudio = AudioBitrate.bitrate_160kbps;
                    break;
                case "192 kbps":
                    debitAudio = AudioBitrate.bitrate_192kbps;
                    break;
            }

            AudioChannels canaux = AudioChannels.Stereo;

            switch ((string)j["AudioOptions"]["Channels"])
            {
                case "Stereo":
                    canaux = AudioChannels.Stereo;
                    break;
                case "5.1":
                    canaux = AudioChannels.FivePointOne;
                    break;
                case "Mono":
                    canaux = AudioChannels.Mono;
                    break;
                default:
                    break;
            }

            BitrateControlMode modeDebit = BitrateControlMode.CBR;

            switch ((string)j["VideoOptions"]["BitrateMode"])
            {
                case "UnconstrainedVBR":
                    modeDebit = BitrateControlMode.UnconstrainedVBR;
                    break;
                case "CBR":
                    modeDebit = BitrateControlMode.CBR;
                    break;
                case "Quality":
                    modeDebit = BitrateControlMode.Quality;
                    break;
                default:
                    break;
            }

            H264Profile profilH264 = H264Profile.Main;

            switch ((string) j["VideoOptions"]["EncoderProfile"])
            {
                case "Baseline":
                    profilH264 = H264Profile.Baseline;
                    break;
                case "Main":
                    profilH264 = H264Profile.Main;
                    break;
                case "High":
                    profilH264 = H264Profile.High;
                    break;
                default:
                    break;
            }

            MouseDetectionMode modeDetectionSouris = MouseDetectionMode.Hook;

            if ((string)j["MouseOptions"]["MouseClickDetectionMode"] == "Polling")
            {
                modeDetectionSouris = MouseDetectionMode.Polling;
            }

            RecorderOptions options = new RecorderOptions
            {
                RecorderMode = RecorderMode.Video,
                IsThrottlingDisabled = (bool)j["IsThrottlingDisabled"],
                IsHardwareEncodingEnabled = (bool)j["IsHardwareEncodingEnabled"],
                IsLowLatencyEnabled = (bool)j["IsLowLatencyEnabled"],
                IsMp4FastStartEnabled = (bool)j["IsMp4FastStartEnabled"],
                AudioOptions = new AudioOptions
                {
                    Bitrate = debitAudio,
                    Channels = canaux,
                    IsAudioEnabled = (bool)j["AudioOptions"]["IsAudioEnabled"],
                    IsInputDeviceEnabled = (bool)j["AudioOptions"]["IsInputDeviceEnabled"],
                    AudioInputDevice = (string)j["AudioOptions"]["AudioInputDevice"]
                },
                VideoOptions = new VideoOptions
                {
                    BitrateMode = modeDebit,
                    Bitrate = (int) j["VideoOptions"]["Bitrate"],
                    Framerate = (int) j["VideoOptions"]["Framerate"],
                    IsFixedFramerate = (bool) j["VideoOptions"]["IsFixedFramerate"],
                    EncoderProfile = profilH264
                },
                MouseOptions = new MouseOptions
                {
                    IsMouseClicksDetected = (bool) j["MouseOptions"]["IsMouseClicksDetected"],
                    MouseClickDetectionColor = (string) j["MouseOptions"]["MouseClickDetectionColor"],
                    MouseRightClickDetectionColor = (string) j["MouseOptions"]["MouseRightClickDetectionColor"],
                    MouseClickDetectionRadius = (int) j["MouseOptions"]["MouseClickDetectionRadius"],
                    MouseClickDetectionDuration = (int) j["MouseOptions"]["MouseClickDetectionDuration"],
                    IsMousePointerEnabled = (bool) j["MouseOptions"]["IsMousePointerEnabled"],
                    MouseClickDetectionMode = modeDetectionSouris
                }
            };

            return options;
        }

        /// <summary>
        /// Permet de convertir une couleur System.Drawing.Color en couleur hexadécimale (string).
        /// </summary>
        /// <param name="couleur">La "Color" à convertir.</param>
        /// <returns>La chaîne de caractère qui représente une couleur en format hexadécimal.</returns>
        private string EnHex(Color couleur)
        {
            return "#" + couleur.R.ToString("X2") + couleur.G.ToString("X2") + couleur.B.ToString("X2");
        }

        #endregion
    }
}
